VAR health = 80.0

VAR mood = 50
VAR maxHealth = 90.0
VAR gold = 0
VAR goldWon = 10
VAR enemy = "empty"

VAR image = "empty"
VAR strength = 0
VAR dexterity = 0
VAR intelligence = 0
VAR luck = 0
VAR wisdom = 0
VAR constitution = 0
VAR charisma = 0

VAR debuffBleed = 0
VAR bleedDamage = 3

//For card collection screen
VAR cardSelected = "empty"
VAR cardStory = "empty"
VAR tempEnemyName = "empty"

//set to 0 when playing in inky
VAR playInky = 1

// trinkets will actually be cards. Possibly one use cards
LIST Trinkets = Wooden_Spike, Other_Object


LIST SpecialCards = (nothing), Sionroths_Blessing, Red_Mans_Finger, Serrated_Dagger, Warts_Card, Blood_Of_Paradai, Church_Beasts_Mask, Bell_Of_Paradai, Barons_Crow, Grieving_Heart

-> set_statsUnity
//-> set_stats


=== function print_num(x) ===
{ 
    - x >= 1000:
        {print_num(x / 1000)} thousand { x mod 1000 > 0:{print_num(x mod 1000)}}
    - x >= 100:
        {print_num(x / 100)} hundred { x mod 100 > 0:and {print_num(x mod 100)}}
    - x == 0:
        zero
    - else:
        { x >= 20:
            { x / 10:
                - 2: twenty
                - 3: thirty
                - 4: forty
                - 5: fifty
                - 6: sixty
                - 7: seventy
                - 8: eighty
                - 9: ninety
            }
            { x mod 10 > 0:<>-<>}
        }
        { x < 10 || x > 20:
            { x mod 10:
                - 1: one
                - 2: two
                - 3: three
                - 4: four        
                - 5: five
                - 6: six
                - 7: seven
                - 8: eight
                - 9: nine
            }
        - else:     
            { x:
                - 10: ten
                - 11: eleven       
                - 12: twelve
                - 13: thirteen
                - 14: fourteen
                - 15: fifteen
                - 16: sixteen      
                - 17: seventeen
                - 18: eighteen
                - 19: nineteen
            }
        }
}

=== function mobName(x) ===

{
    - x == "Bandit":
        {~Scruffy|Dirty|Ragtag|Shabby|Moth-Eaten} Bandit
    - x == "Blob_Beast":
         {~Gooey|Slimy|Clammy} Blob
    - x == "Brazen_Filth":
        {~Dirty|Ragtag|Shabby|Moth-Eaten|Scrawny|Mangy|Squalid} Brazen Filth
    - x == "Church_Beast":
         ~ return "Church Beast"
    - x == "Crow_Man":
        {~Pretentious|Sweaty|Repulsive} Baron
    - x == "Flour_Witch":
         ~ return "Flour Witch"
    - x == "Giant_Pig":
         ~ return "Giant Pig"
    - x == "Goblin":
        {~Scruffy|Dirty|Scrawny|Nasty|Mangy|Squalid|Moth-Eaten} Goblin
    - x == "Old_Man":
         ~ return "Old Man"
    - x == "Smoke_Beast":
         ~ return "Smoke Beast"
    - x == "Wart":
         ~ return "Wart"    
    - x == "Wisp":
         ~ return "Wisp"
    - x == "Zombie_Dog":
        {~Large|Mangy|Frothing} Zombie Dog
} 

=== function alterStat(ref x, y) ===
// adjusts variable x up or down by y
   ~ x = x + y


=== function alterHealth(x) ===
    // limits health to a maximum maxHealth
 //   You {x<0:
   //         <>lose {x} health
     //   - else:
       //     <>gain {x} health
//        }
    { health + x > maxHealth:
        ~ health = maxHealth
    - else:
        ~ health = health + x
    }
    
    { health < 0:
        ~ health = 0
    }
        
  //  <>. You now have {health} health 
    
=== function alterGold(x) ===
    // gold can't go below 0
    
    { gold + x < 0:
        ~ gold = 0
    - else:
        ~ gold = gold + x
    }


=== function returnHealthText ===
// used to return a random description of ailments based on percentage of health. Based on a sentance similar to "The tea seems to help ... and you feel ... better"
~ temp healthPercentage = health/maxHealth*100.0
{
- healthPercentage<10:
    {~staunch the bleeding|with the blood loss}
- (health/maxHealth*100)<20:
        health between 10 & 20%
- health/maxHealth*100<30:
        health between 20 & 30%
- health/maxHealth*100<40:
        health between 40 & 40%
- health/maxHealth*100<50:
        health between 40 & 50%
- health/maxHealth*100<60:
        health between 50% & 60%
- health/maxHealth*100<70:
        health between 60% & 70%
- health/maxHealth*100<80:
        health between 70% & 80%
- health/maxHealth*100<90:
        {~ throbbing headache|sore arms}
- health/maxHealth*100<100:
       your {~aches|soreness|aches and pains}
}

===function bodyDescription===
//{health<maxHealth-10: your legs ache as} you move quickly down the road ahead. 
// ... as you move quickly down the road
~ temp healthPercentage = health/maxHealth*100.0
{
- healthPercentage<10:
    your {~side is bleeding heavily|body can barely take it}
- (health/maxHealth*100)<20:
        your health between 10 & 20%
- health/maxHealth*100<30:
        your health between 20 & 30%
- health/maxHealth*100<40:
        your health between 40 & 40%
- health/maxHealth*100<50:
        your health between 40 & 50%
- health/maxHealth*100<60:
        your health between 50% & 60%
- health/maxHealth*100<70:
        your health between 60% & 70%
- health/maxHealth*100<80:
        your health between 70% & 80%
- health/maxHealth*100<90:
        your {~head throbs|arms ache|legs ache}
- health/maxHealth*100<100:
        your {~leg aches|arms are sore}
}



=== function returnHealthTextAfterFight ===
// the fight has left you ...
~ temp healthPercentage = health/maxHealth*100.0
{
- healthPercentage<10:
    {alterStat(mood, -50)}
    {~badly hurt| bleeding badly|near deaths door}
- (health/maxHealth*100)<20:
        {alterStat(mood, -30)}
        {~unsure if you can carry on|fearful of the future}
- health/maxHealth*100<30:
        {alterStat(mood, -20)}
        health between 20 & 30%
- health/maxHealth*100<40:
        {alterStat(mood, -10)}
        health between 40 & 40%
- health/maxHealth*100<50:
        {alterStat(mood, -1)}
        health between 40 & 50%
- health/maxHealth*100<60:
        {alterStat(mood, 1)}
        exhausted
- health/maxHealth*100<70:
        {alterStat(mood, 3)}
        health between 60% & 70%
- health/maxHealth*100<80:
        {alterStat(mood, 5)}
        health between 70% & 80%
- health/maxHealth*100<90:
        {alterStat(mood, 8)}
        {~ with a throbbing headache|with sore arms}
- health/maxHealth*100<95:
        {alterStat(mood, 9)}
        {~sore, but confident|with aching arms|with minor aches and pains}
- health/maxHealth*100<=100:
        {alterStat(mood, 10)}
        {~feeling confident of your ability|confident of the future}
}

=== function feelBetter(x) ===
//and you feel ... "
~ temp tempHealth = health + x //to check what health will be once healing included
{
    
    - tempHealth==maxHealth:
    {~fully restored|ready for future fights}
    - x<=5:
    {~slightly |a bit |marginally |somewhat }{~improved|better}
    - x<=10:
    
    - x<=20:
    {~a lot|considerably|much better|}
    - x<=30:
    
}


=== function addCard(x) ===
    This card was added to deck: {x}
    ~ SpecialCards += x
    ~ SpecialCards -= nothing

=== Shop_Scene(-> point_to_return, name_of_shop)
# Shop
->End_Shop(point_to_return)

===End_Shop (-> point_to_return) ===
+ [End Shop]
-> point_to_return   
    
    
    
//=== Fight_Scene(-> point_to_return, name_of_enemy)
//    + [Attack {mobName(name_of_enemy)}]
//    -> Fight_Scene_noOps(point_to_return, name_of_enemy)
    

=== Fight_Scene(-> point_to_return)
   
// 1 means playing in unity - take out this if statement for final version
    { playInky == 1:
        + [End Fight]
        -> point_to_return
    - else:
    + [End Fight]
    -> End_Fight(point_to_return)
    }

=== End_Fight(-> point_to_return) ===

    Some debug options 
    + [End fight health unchanged]

    + [End fight lose 20 health]
    ~ health = health - 20
    + [End fight lose half health]
    ~ health = health/2
    + [End fight deaths door - 5 health left]
    ~ health = 5

-

-> point_to_return
    
    


    
===set_stats===
=initial_stats
Current Stats:
Strength (Affects Damage): {strength}
Dexterity (Affects Block): {dexterity}
Intelligence (Affects number of cards drawn): {intelligence}
Luck (Gold and card rarity): {luck}
Wisdom (retain cards): {wisdom}
Constitution (Health): {constitution}
Charisma (Cheaper items): {charisma}
//{initial_stats}
+ {initial_stats <= 4} [Increase Strength] {alterStat(strength, 1)} ->set_stats
+ {initial_stats <= 4} [Increase Dexterity] {alterStat(dexterity, 1)} ->set_stats
+ {initial_stats <= 4} [Increase Intelligence] {alterStat(intelligence, 1)} ->set_stats
+ {initial_stats <= 4} [Increase Luck] {alterStat(luck, 1)} ->set_stats
+ {initial_stats <= 4} [Increase Wisdom] {alterStat(wisdom, 1)} ->set_stats
+ {initial_stats <= 4} [Increase Constitution] {alterStat(constitution, 1)} ->set_stats
+ {initial_stats <= 4} [Increase Charisma] {alterStat(charisma, 1)} ->set_stats
* {initial_stats > 4} [Start Game] -->introduction

===set_statsUnity===
//~strength = 10
//~dexterity = 2
//~intelligence = 1
//~luck = 2
//~wisdom = 0
//~constitution = 0
//~charisma = 0
->introduction

===introduction ===
~ image = "c1_start_landing"
You were raised and trained for a purpose, all the other people living around you on the fortified island are there either to serve you, or teach you. You learn ways of physical combat and the use of powerful trinkets, as only one with the royal blood can vanquish the cursed king and remove the scourge of evil from the land and return it to its former glory, thus, this is the task you have been assigned from birth.
You are equipped with charms and runes of empowerment and protection, and given a sturdy blade and a suit of armour, ushered onto a small boat with a ritualistic celebration, the hope of the people clear on the faces of the crowd lining the waters edge, as you push off into the sea towards the mainland. 


*Chapter 1
-> Chapter1

=== Chapter1 === 
~ image = "c1_start_landing"
~ enemy = "Brazen_Filth"
~ tempEnemyName = "{mobName(enemy)}"
Monstrous waves cast you violently to the rocky shore, smashing your small boat on the dilapidated stone quay, {strength > 2: Glad of your strength you stagger to your feet. T|struggling to your feet, t}he wind howling and lashing rain against your armour. You have been badly bruised by the landing and you know you should try and find cover to wait out the rest of the storm. 

Looking around the closest structure you can make out in the briney spray and gusting sheets of water seems to be a small barn. A little further down you can just about see another structure, it might be an inn, as a square sign outside batters against its post and wrenches to and fro in the gale. 
You can stumble into the barn which looks dark and uninviting, go a bit further to investigate the inn and maybe meet some locals or attempt to wait out the storm on the beach.
*(beach1)[Wait out the storm]
The storm is long and violent. Setting up a small tent to protect from the lashing rain you attempt to enter a medative state. Dark thoughts of the task ahead take over and you find any sort of relaxation impossible. 
You need to decide whether to stay on the beach or enter one of the buildings.
    **(beach)[Continue on beach]
    Determined to overcome the dark thoughts you attempt to re-enter your medative state. After what seems like hours, but was most likely only minutes you find yourself relaxed, calm and able to enter a deep sleep. However within minutes you are woken by a scrawny figure trying to cut your purse from your belt. Still feeling dazed from the lack of sleep you pull out your sword and attack the brazen filth. 
        ***[Attack {tempEnemyName}!] 
        # Fight #regular #normal #Contrive #Sionroths_Blessing
        ~ cardStory = tempEnemyName + " is dead"
        
        ->Fight_Scene(->dead_brazen_filth) 
    **[Enter the barn]
    ->barn
    **[Enter the inn]
    ->inn

*(barn)[Investigate the barn]
~ image = "c1_barn"
    You crash into the barn, stumbling and almost falling over yourself as the large door gives way and lets you in. In the darkness you can make out a few shapes, finding a few bales of soft mildewy hay, you collapse exhausted and sleep overcomes you. {alterHealth(5)}
        ->barn_filth
    **[Leave Barn] 
    ->Morning1

*(inn)[Investigate the inn]
~ image = "c1_inn"
Crashing into the inn, the sign announcing it as the broken flagon. You are greeted with silence. A few dim guttering candles barely illuminate the place, slow slinking figures, drift into the shadows leaving you alone in the room. The place has been ransacked, the bar smashed and only the jagged stumps of bottles remain. A few tables and rickety chairs are scattered about… The air is foul, a slight hint of mouldy ale, briney mud and subtle fishy tang. There is no hospitality here, no locals drinking, seeking respite from the weather, no hot food to be had. You are exhausted, will you attempt to find somewhere to sleep or give in to the temptation and just find a seat in a corner to pass out on. {alterStat(mood, -5)}

**(find_sleep)[Find somewhere to sleep]
    You decide to make a bit more effort to find somewhere to rest yourself, after a bit of investigation you find a rickety staircase partially concealed by a barricade of upturned tables. Once removed, you go up the stairs, and find several small rooms with bedrolls inside. You enter one, block the doorway by sliding a chest of drawers across, and pass out on a mildewy bedroll.
    You awake feeling refreshed. {alterHealth(10)}{alterStat(mood, 3)}
    *** [Venture Downstairs]
    At the bottom of the stairs, blocking your way <>

**(huddle)[Huddle up in the corner]
    Overcome with exhaustion you decide to just huddle best you can in the corner, you make yourself as comfortable as you are able, and sleep takes you over.{alterHealth(2)}{alterStat(mood, -5)} 
    ***[Wake up]

It is light when you awake, the weather seems to have calmed, the sign creaks and water drips from the ceiling and down some of the walls. Surrounding you <>

- are several crouched scrawny figures, with large luminous eyes. They startle and shift nervously back as you awake, grimacing revealing stubs of jagged teeth… One grabs a broken chair leg and brandishes it aggressively… You draw your sword, all the other creatures scatter at the sight of the naked steel, all apart from the brazen filth with the chair leg, it moves to attack. 
    *[Attack {tempEnemyName}!] 
        ~ cardStory = tempEnemyName + " is dead"
        # Fight #regular #normal #normal #Red_Mans_Finger
        ->Fight_Scene(->dead_brazen_filth) 
=barn_filth

You are woken by a thump and you realise that the barn door is swinging open. Rolling over you see a scrawny figure creeping towards you. Reaching for your sword you can attack the creature or attempt to flee out the door.
    *[Attack {tempEnemyName}!] 
    ~ cardStory = tempEnemyName + " is dead"
    # Fight #mob_halfHealth #normal #normal #normal
        ->Fight_Scene(->dead_brazen_filth) 

    *[Flee]
    The creature is faster than you realised and jumps into your path. You have no choice but to attack it.
        **[Attack {tempEnemyName}!] 
        ~ cardStory = tempEnemyName + " is dead"
        # Fight #regular #normal #normal #Red_Mans_Finger
        ->Fight_Scene(->dead_brazen_filth) 
    
=dead_barn_filth

->Morning1

=dead_brazen_filth
Gingerly you poke the dead body with your sword, hesitant whether or not to search it for gold and useful items. Its flea ridden body almost seems alive due to the amount of small creatures which inhabit it. Would you like to investigate it further, or leave it?
    *Examine creature
    Using your sword to pull away at the clothes on the creature you discover a hidden pouch. Opening it you find {print_num(goldWon)} gold coins which you quickly take. Hoping that you haven't picked up anything else more malignant you leave the filth <>
    *[Leave]
    Deciding against getting too close to the dead body you leave the filth <>
-
{barn:on the floor and glance out of the barn door. The storm has abated and you <>}
{inn:in the dirt and confident that the rest of the inn's inhabitants won't follow, <>}
{beach:lying on the sand. }
{not beach:emerge into the daylight.} The fight has left you {returnHealthTextAfterFight()}, {health==maxHealth:and ready for the next challenge. | and you are glad of the fresh air to help clear your mind. }

->Morning1

=Morning1
~ image = "c1_old_lady_shelter"

Now the storm has passed the sea is languid. Its sickly sluggish waters lap at the coast, the ground is little more than churned mud, water drips from all the structures. Looking inland you can see your destination, the twisted spire of the king's citadel, jutting out of the clouds rushing across the mountainous ridge. Between you and the citadel lie many miles of treacherous landscapes and accursed beasts. 
{not inn: Glancing through the inn doors you see the place has been ransacked and decide not to go any further in. }Further down the path inland, there is a stone fountain, a statue of a hunched man, eyeless and tortured. At the base of the fountain is a ragged shelter, heavy cloth flapping in the wind, bound sticks holding it up, as you get closer, you can see an old lady, busying herself with a pestle and mortar
    * [Greet old lady]
    She looks up as you approach… “ooo my, I have not seen anyone around these parts for ages… Wait… mmmm… You are… I can sense your lineage, your blood, you are a royal heh heh, are you going to save us? Come in, let me get a gooood look at you.” 
    {health<maxHealth: "I may be able to help you with those wounds of yours." }She reaches out to take your arm. Would you like to enter the shelter, or move on?

    **(speak_to_lady)[Enter shelter]
        Using your arm to support herself the lady makes some space and gestures for you to sit. "It's going to be a hard journey for you{dead_brazen_filth: and I can see that you've already encountered one of the many inhabitants of this area}, would you care for some tea?" 
        She offers you some foul smelling sepia coloured tea. 
        ***(tea)[Accept tea]
        
        It is as foul as it smells bitter, your mouth goes numb. {health<maxHealth: The tea seems to help {returnHealthText()} and you feel {feelBetter(5)}. You wonder if you can ask for more.| It seems to lift your mood. }
        {alterHealth(5)}
        {alterStat(mood, 15)}
        ***[Refuse tea]
        {health<maxHealth:“Oh dear“ she cackles, “We are not going to get far with that attitude”{alterStat(mood, -5)}|"Well I suppose you didn't need it anyway"}
    --- “There are some urrr, artifacts that the His royal majesty Sadul fears, mmmm, and he has cast them far away from his sight, often guarded, sometimes hidden. I urge you to seek some of these if there is a chance for you to be victorious. To the east, is the Cruelroot grove, there you might find the Bell of Paradai, lots of wild beasts and unkindness, to the west scallions and brigands, I hear that the Red Man's Finger is there… But you should head to Harless Manor after. Heh heh heh, a small chance…”
    {tea && health<maxHealth: You turn to ask for some more tea but find yourself being shepherded out of the shelter|She gestures you to leave and returns to her grinding, chuckling and crooning to herself. } 
     
    ***[Leave shelter]
    Leaving the old lady to her muttering you continue down the path.->crossroad

**[Move on]
You hurry past, avoiding her touch and hear her cackling behind you  “We are not going to get far with that attitude”. Leaving her to her muttering {bodyDescription()} you move quickly down the road ahead. 
->crossroad

* [Hurry by]
You skirt around the hut, avoiding looking at the old lady. {health<maxHealth: The old ladies cackling and laughing as she sees you hurrying by makes you move even faster. | You hear her call out to you as you pass, “We are not going to get far with that attitude.”}
->crossroad

=crossroad
//~ image = "c1_cross_road"
- ~ image = "c1_cross_road"
~ enemy = "Goblin"
~ tempEnemyName = "{mobName(enemy)}"
Ahead is a path that splits into two, trees line the sides of the road, all twisted and scrawny. Tough thorny scrub coats the ground, ahead an old finger post sags, pulled down by dried vines.
A pack of hateful looking creatures are fighting each other over an animal carcass, they snarl and hiss at each other. Scrawny beasts, all sinew and elbows, black dead eyes and a long snout, jaws full of thin crystalline teeth. They catch sight of you, and stop. The largest one scuttles towards you on all fours and leaps to attack. 
    
    *[Attack {tempEnemyName}!] 
    ~ cardStory = tempEnemyName + " is dead"
        # Fight #regular #normal #normal #normal
        ->Fight_Scene(->dead_goblin) 


=dead_goblin
With your final blow the paltry Goblin collapses to the ground at your feet, the others cringe away from you and slink away into the bushes. You can feel their eyes on you as you wipe the goblins blood from your sword. The fight has left you {returnHealthTextAfterFight()} and you wonder how many more of these creatures stand between you and your goal. Looking down at the body you are unsure if you should examine it further or leave the area before the other Goblins become braver.
    *(examine_goblin)[Examine Body]
        It's slimy and there's blood oozing over the rags it was wearing. Buried in the rags you find a few coins. Cleaning them of blood, you add the {print_num(goldWon)} gold coins to your own.
        Looking around you see the signpost, removing the dead vines you can see it has writing on it. ->crossroadpart2
    *[Read Sign]
        Leaving the Goblins body for the moment you examine the signpost. It's covered in dead vines. Pulling them off you can read the writing underneath. ->crossroadpart2
->crossroadpart2

=crossroadpart2

The sign points out two locations… Lothaire village to the west, and Cruelroot Grove to the east. {speak_to_lady: You remember the old lady told you of artifacts that you might find in these places, the Bell of Paradai in Cruelroot Grove and the Red Man's Finger somewhere to the West. You wish the old lady had given you more information about what these artifacts might do.| With no information to go on you will have to let intuition guide your way}

* {not examine_goblin}Examine Goblin body
    You take a closer look at the Goblin's body. There is blood oozing from its wounds. Buried in the rags it was wearing you find a few coins. As you're cleaning them of blood you hear a noise behind you. Just in time you turn around to see two smaller Goblins about to jump you. Drawing your sword you move to block the attack. 
        ~ enemy = "Goblin" 
        ~ tempEnemyName = "{mobName(enemy)}" //remake goblins name
        **[Attack {tempEnemyName}s!] 
        ~ cardStory = tempEnemyName + "s are dead"
        # Fight #two #normal #normal #normal
        ->Fight_Scene(->dead_goblin2) 


*[Go West towards Lothaire Village] 
->West

*[Go East towards Cruelroot Grove] 
->East

=dead_goblin2
Deciding to take no chances this time you quickly search the Goblins body taking the {print_num(goldWon)} coins that you find. Looking up at the signpost you know you need to make a decision. East to Cruelroot Grove {speak_to_lady:and the Bell of Paradai }or West to Lothaire village {speak_to_lady: and hopefully the Red Man's Finger}  
*[Go West] 
->West

*[Go East] 
->East

=West

After traveling for a few miles along the dirt tack, the trees thin out, and you can see over some scrubby fields of desiccated corn, in the distance the blades of a windmill. The road dips down and into a tight copse of trees.  

Do you want to follow the road, or skirt around the edge of the copse. 

*[Follow the road]
~ image = "c1_copse_pig"
~ enemy = "Giant_Pig"
~ tempEnemyName = "{mobName(enemy)}" 

The light dims and goes a sickly green under the shadow of the tightly packed trees. In the middle of the road is a large decaying pigs head secured on a spike, the buzzing of flies is audible as you get closer. Examining the porcine remains you can see that it has been there for a while, at the base of the stake are some flowers and leather pouches arranged as if it was a shrine.  
    **[Examine pouches]
        //Examine pouches. 
        Opening up the pouches reveals a collection of {print_num(25)} gold coins. Looking around there is no one in sight. Pausing for a moment you decide if you should take the coins. 
        {print_num(health)}
        ***(take_pig_coins)[Take coins]
            Reaching into the pouch and taking out the coins you count them and discover there are more than you first realised. You add {print_num(50)} gold coins to your own. A noise behind you makes you turn around quickly. The expression on the pigs head has changed to one of anger and a dull pain starts in your hands. Watching them you realise bleeding boils are developing over all your hands. You will need the strength of combat to remove this.{alterGold(50)} {alterHealth(-bleedDamage)}
            ~ debuffBleed = 3

        ***(leave_pig_coins)[Leave coins]
            Wary of the shrine and it's powers you return the pouch to the shrine. A noise from the pigs head startles you and turn to see the expression on the pig has changed to one of satisfaction. A warm glow engulfs your body giving you a feeling of confidence and strength. This will aid you during your next fight{alterHealth(5)}

        --- Turning away from the shrine you start back on the road, a large pig bursts out of the trees, it's slick with sweat, and numerous sweltering boils dot its skin. Riding on the back of the giant swine is a small stick like figure, shaking its gnarled fists at you. The pig attacks. 
            
            ****[Attack {tempEnemyName}!]
            ~ cardStory = tempEnemyName + " and shrunken stickman are dead"
            {
                - take_pig_coins:
                # Fight #debuffBleed #normal #normal #normal
                - leave_pig_coins:
                # Fight #buffStrength #normal #normal #normal
                - else:
                # Fight #regular #normal #normal #normal
            }
            
            ->Fight_Scene(->dead_giant_pig) 

    **[Carry on]
    ~ image = "c1_shack"  
    Erring on the side of caution, you leave the shrine alone, and eventually you break free of the shadowy copse. <>
    -> eerie_shack

*[Skirt around the edge]
~ image = "c1_corn_field"
~ enemy = "Bandit"
~ tempEnemyName = "{mobName(enemy)}" 

Going around the side of the copse, you enter one of the dessicated corn fields. The corn is high and you make your way through it cautiously. The corn parts and you can see a trodden down clearing, a raggedy man, clad in leather armour and scraps of cloth is rifling through the packs of a corpse on the floor. 

He has not heard you.
You can attack and attempt to surprise him, wait to see what he does, or re-enter the corn and leave him to it. 

 
    
    **(fight_in_corn)[Attack {tempEnemyName}!]
     ~ cardStory = tempEnemyName + " is dead"
        # Fight #mob_dazed #normal #normal #normal
        ->Fight_Scene(->dead_bandit) 

    **[Wait]
    The Bandit, takes several items off of the mans pack, gives the corpse a kick and slinks off into the corn without looking back. 
        ***[Examine corpse]
        TODO 
        There is nothing left on the corpse? - or maybe something that damages you? OR you find something the bandit has missed OR the man could attack you from behind. 
            ****[Continue]
    
        ***[Continue]

    **[Move on] 
    You carry on pushing through the dried stems. 
    After you go around a little further you break out of the corn as you do so some daggers fly at you from behind, each one hitting you and causing you to stumble. {alterHealth(-10)} The bandit leaps out of cover and attacks you snarling obscenities and brandishing two daggers. 
        ***[Attack {tempEnemyName}!]
        ~ cardStory = tempEnemyName + " is dead"
        # Fight #player_AP2 #normal #normal #normal
        ->Fight_Scene(->dead_bandit) 


    --You push your way through the corn, after a while you think you can see the edge… A harsh guttural voice rings out “Drop your gold on the ground, and I will let you go on your way!”
    
    Will you place all your gold on the ground, or turn to attack?
    
        **[Drop gold]
        You hear a soft chuckle… “Much appreciated, now leave.”
        You break free of the corn and can see the edge of the copse and the trail that continues onwards.
            ***[Follow trail]
            ->Lothaire_village
        
        **[Attack {tempEnemyName}!] 
        ~ cardStory = tempEnemyName + " is dead"
        # Fight #regular #normal #normal #normal
        ->Fight_Scene(->dead_bandit) 


=dead_bandit

Taking the bandits purse you continue on. {alterGold(goldWon)}

{fight_in_corn: 
<> Pushing through the corn you eventually come to the edge of the field and break out of the corn onto a small path. ->eerie_shack

- else:
Breaking free of the corn the edge of the copse if visible and the trail continues onwards.
}

-> eerie_shack


=dead_giant_pig
    Examining the stickman that rode the pig you find some gold coins. Quickly adding these to your purse you leave the pig and the stickman on the side of the road. 
        -> eerie_shack

=eerie_shack
~ image = "c1_shack"
 Following the path, it goes over a small rise, at the top you can see a small rickety shack. There is a series of tall spindly figures, made from straw and bones, struck into the earth with solid stakes, skulls turned towards the shack, crude arms outstretched.  As you get closer a low hum starts to emanate from within the shack. A flock of crows nearby startle, flying off cawing roughly. Do you want to investigate the shack? Or leave it alone. 


*[Investigate]
//investigate

You push open the door to the shack with a eerie creak. The hum abruptly stops. Inside is several rows of benches. Many wretched human figures sit on the benches, rocking slowly back and forth, clothed in rags, wearing crowns of twigs upon their heads. As you enter, they all turn to face you. Each face curved in your direction has has a grotesque wooden mask on, dark round eyes and mouths agape. They start to hum, gaunt pale arms reaching towards you as if in welcome. From the dark recesses of the shack, a figure shambles forward. It wears a long dark cloak, no limbs are visible, the edges ragged, drag on the floor. The head is a stags skull, mossy and buzzing with flies. 
    **[Step Forward]
    “Welcome.” it says, its voice hollow and low, “We have prayed and waited so long for one such as you to arrive, it is a glorious occasion for you to grace us finally. All we require from you, to bring the prophecy to fulfillment is some of your essence. Give freely what we require, and go with the blessing of Sionroth, Just a little blood. That is all.” 
        Would you agree, and offer some blood, or leave?

        ***[Offer Blood]
             “Excellent.” He whispers. Brandishing a curved blade, and a metal bowl from the hollows of his robe.
            Lose x max hp (10  might be too much) gain card. Sionroths blessing. A card that draws another card and gives you 2 ap for that turn (costs 0)
            {alterHealth(-10)}
            {addCard(Sionroths_Blessing)}

            The bowl containing your blood withdraws into the folds of the cloak, and the figure retreats once more into the shadows. 

            “May Sionroth help you on your journey, we are grateful for this sustenance.”

            The congregation turn once more to the front and the humming starts to gain in pitch. 
            ****[Leave]
            You turn to leave, quietly closing the door behind. The humming stops. 
            The stick figures that were outside, are no longer there, only the posts remain.
            *****[Continue Along Path]
                -> Lothaire_village
        ***[Leave]
        As you turn to leave, the voice whispers, “Such a disappointment, like so many of those before you”
        TODO This connection needs more work
        -> Lothaire_village


*[Leave]
-> Lothaire_village

=Lothaire_village
~ image = "c1_lothaire_village"
~ enemy = "Goblin"
~ tempEnemyName = "{mobName(enemy)}" 

Following the path you reach Lothaire village and its tattered mill. 
The village appears to be obliterated, just the stubs and outlines of buildings remain, dry tangled vines cover what is left.
 The windmill is a stout stone structure, the sails no longer turn, the cloth ripped and stained. The area leading upto it has a few small outbuildings and a low stone wall encircles it. 
 *[Enter Village]
Leaping over the wall, you can see that the ground is churned mud, bits of corpses are visible protruding out, human and animal… there is a horse, caked in mud lying close to one of the smaller buildings, as you get close, it wearly raises its head and starts to neigh and scream in agony. It is very loud. 
You can put the horse out of its misery or leave it alone

    **[Put down the horse]
        You put a kind hand on the filthy animal, its breathing is shallow and panicked. With a swift blow you end its misery and silence falls upon the area. 
        TODO Maybe get a card here - some kind of humanity card
        -> Investigate_Village
    
    **[Leave it alone]
        From outside one of the outhouses a dirty goblin crawls, It eagerly approaches the horse, drawn by its cries and suffering, it spies you, recoils back… Than gathers up its courage and attacks.->fight_horse_goblin
    
=fight_horse_goblin

*[Attack {tempEnemyName}!]
~ cardStory = tempEnemyName + " is dead"
# Fight #regular #normal #normal #normal
->Fight_Scene(->dead_horse_goblin) 

        
=dead_horse_goblin
    As the goblin falls into the mud, the horse finally breathes its last and silence falls upon the area. 
->Investigate_Village

=Investigate_Village

You can investigate the outhouses or the mill

*[Investigate outhouses]
    -> visit_outhouse
*[Investigate mill]
    -> visit_mill


=visit_outhouse
    The first building is mostly dark, smells dank and musty, small shafts
    of light come through holes in the roof, <>

    {fight_horse_goblin: 
            its empty of life, there is a small nest of cloth and sacking, probably where the goblin was sleeping. Searching the nest, you find 10 pieces of gold. {alterGold(10)} 
                Crossing ->after_outhouse
    - else:
        sleeping in a small nest of cloth and sacking there is a filthy goblin. Seeing the opportunity to gain an advantage, you stealthily approach, and when close enough slash viciously upon the sleeping creature.
            ~ enemy = "Goblin"
            ~ tempEnemyName = "{mobName(enemy)}" 
            *[Attack {tempEnemyName}!]
            ~ cardStory = tempEnemyName + " is dead"
            # Fight #regular #normal #normal #normal
            ->Fight_Scene(->dead_horse_goblin2) 
    }

=dead_horse_goblin2
Leaving the dead goblin in it's nest you cross ->after_outhouse

=after_outhouse
- the muddy yard and peer into the other outhouse. It is empty and ruined, it seems to be the remains of a stable.

You can carry on down the path or investigate the mill.
*[Investigate mill]
    ->visit_mill
*[Carry on]
    ->Edge_of_Village

=visit_mill
~ image = "c1_mill_interior"
~ enemy = "Flour_Witch"
~ tempEnemyName = "{mobName(enemy)}" 
    The heavy wooden door is stuck fast, warped into the heavy frame, with a shove it opens with a deafening shriek of rusted hinges.  
    The inside is dimly lit from a few holes further up, motes of dust drift down and swirl around. The floor is coated with a deep white carpet of flour. In the center of the mill lies the grindstone, torn off its seating, the center pole askew, all coated in the white dust. 
    Staggering out of the white miasma is a staggering female figure, she looks hurt, she lurches towards you, hunched over hand outstretched, long dark hair falling over her face… As she comes closer large taloned malformed limbs reach out of the cloak, the hair falls back and where you expected to see her face, mandibles dark and sinewy coil free, she issues a hideous chattering noise and leaps to attack.
    
    *[Attack {tempEnemyName}!]
    ~ cardStory = tempEnemyName + " is dead"
    # Fight #regular #normal #normal #normal
    ->Fight_Scene(->dead_flour_witch) 

=dead_flour_witch
    When the swirling dust settles the monstrosity lies dead and ticking at your feet. Upon her body, tied around her neck on a piece of twine, there is a glass vial. Picking it up you can see, inside is a finger, dark red with a pointed nail. 
     //   {addCard(red_mans_finger)}
    Further away you can make out the squat form of Harless Manor, a thick mist drifts at its base and the skeletal forms of twisted trees enshroud it. 
        ***[Carry on down path]
        ->Edge_of_Village

=Edge_of_Village
~ image = "c1_lothaire_village"
Walking to the edge of the village you hear a chuckling, there is a ruined house, but more intact than the others you have seen here. The walls are barely standing, roof gone, door torn asunder. 

*[Listen to voice]
“Well then my lovelies, just out of reach… heh heh... Is? Is someone there?”
You enter the remains of the house, brick and tiles crunching underfoot. 
There is a huge hole in the floor, a spindly ragged man, with twitchy eye, is frantically wringing his hands and jumping nervously from leg to leg.
“Greetings, not many wandering these parts nowadays, good to meet you, My name is Wart, merchant and purveyor of rare goods… look here… I have found many shiny glittering treasures, behold your eyes” 
He points into hole, the floor boards are bending inwards only darkness visible from where you are. 
“Look, <> ->talk_to_wart

*[Investigate house]
You enter the remains of the house, brick and tiles crunching underfoot. 
There is a huge hole in the floor, a spindly ragged man, with twitchy eye, is frantically wringing his hands and jumping nervously from leg to leg.
“Greetings, not many wandering these parts nowadays, good to meet you, My name is Wart, merchant and purveyor of rare goods…… look here… I have found many shiny glittering treasures, behold you eyes.” 
He points into hole, the floor boards are bending inwards only darkness visible from where you are. 
“Look, <> ->talk_to_wart

=talk_to_wart
{w|W}e are friends now, yes? {I can sell you some of my wares if you wish|Would you like to take another look at my wares}, or if you take a closer look, you can see the riches I have found, I will half them with you… I just cannot see how to reach them.”

Will you

++[Ask {to see what he is selling.|for another look at his wares}]
    -> Shop_Scene(-> talk_to_wart, "wartsShop")

**[Look closer down the hole]

You move across the collapsed and sagging floor, near the hole. Surely enough down in the darkness you can see something glowing, peering closer… Suddenly with a grunt Wart kicks you in the rump sending you sprawling down into the darkness. 
    ***[You are falling]
        You land with a squelch in thick black mud {alterHealth(-10)}
        Wart laughs down at you “Ho ho ho, he he. Now I can pick your corpse for treasures and add to my stock... oooho ho!”
    
    Standing up, and wiping the thick muck from your face you look around. Your eyes are becoming accustomed to the gloom, silhouetted above you is the thin jubilant form of Wart, dancing about, the glow is coming from a sparkling stone set in the top of a large metal banded chest. There seems to be a crude tunnel in one wall. 
    
    ****[Open the chest]
        With a creak you crack open the heavy chest, crammed inside is a small human skeleton, bound up into a tight fetal position, lying on top of it is a wicked looking dagger with nasty serrations.    
            *****[Take the dagger]
            {addCard(Serrated_Dagger)}
            After taking the dagger you <>
            *****[Leave the dagger]
            You <>

    ****[Enter into the tunnel]
    You <>

----squelch through the mire closer to the tunnel, it seems to be keeping its shape through the twisted knotted roots that thread through the loose wet soil… you enter the tunnel, Warts laughter echoing behind you. ->tunnel

=tunnel
~ enemy = "Blob_Beast"
~ tempEnemyName = "{mobName(enemy)}" 
After some time working your way through the tunnel something large is moving slowly towards you. 
It is a huge misshapen blob, it seems to consist of the dark mud and roots, all folds and buboes it moves to attack.
    
    *[Attack {tempEnemyName}!]
    ~ cardStory = tempEnemyName + " is dead"
    # Fight #regular #normal #normal #normal
    ->Fight_Scene(->dead_blob_beast) 

=dead_blob_beast
Your final blow causes the creature to deflate wetly with a disgusting pppprrrrrttt phut noise, and a foul smell washes over you.  
// gold reward
    {alterGold(10)}

Further on the tunnel leads upwards, and you can see daylight, scrambling up a steep slope, grabbing onto roots to help you, you finally get out. The ruined house is not far from you, and you can still see the manor in its shroud of fog. 
You can return to the house to confront Wart or move on towards the Manor. 

***[Confront Wart]
->confront_wart

***[Go to the Manor]
->end_chapter_one

=confront_wart
Entering the ruined house, you can see wart peering nervously down the hole, he whirls around as you approach.
“Oh, by Saduls grace, you are alive! Now lets talk about this, I did not mean it, I did you wrong, I apologise. I get these urges is all, hard to resist the impulses that are whispered to me. Will you forgive a wretch such as myself?”

*[Forgive wart]
    “Oh by the blessings of Batnas, I thank you, a second chance, you are a kind soul… a true ally. Do you want to buy anything? I will offer you a healthy discount.”
    **[Shop]
    -> Shop_Scene (->leaveWart, "wartsShop")
    **(leaveWart)[Leave]
    ->end_chapter_one
*(do_not_forgive)[Do not forgive him]
“Oh look here, just a little jest. Your still alive and fighting fit, I prostrate myself in remorse at your feet… Actually here. A gift, as a symbol of my deepest apologies. “
    {addCard(Warts_Card)}
    -> end_chapter_one

~ enemy = "Wart"
~ tempEnemyName = "{mobName(enemy)}" 
*[Attack {tempEnemyName}!]
~ cardStory = tempEnemyName + " is dead"
# Fight #regular #normal #normal #normal
->Fight_Scene(->dead_wart) 


=dead_wart
Wart’s body falls into the hole, landing with a squelch, a fitting end. 
->end_chapter_one

=end_chapter_one
~ image = "c1_manor_chapter_end"
{do_not_forgive:
With Wart prostrate on the ground you leave the ruined house and <>

-else:
Leaving the ruined house you <>
}
carry on down the path towards the Manor, thick banks of fog drift past obscuring your vision, but eventually you can make out the tall walls and iron gates. ->manor_goblin

=East
The woods close in thick and fast as you walk down the path, tall thick trees, skeletal branches all reaching away from you down the path. The light dims as the the woods grow thicker, the atmosphere heavy and brooding. All the trees seem to be bending towards your destination. A particular tree catches your attention, something about the bark on the trunk, almost like a face. There's a dark hole where the mouth would be. 

Further on you can see what might be a building between the trees. 
    *[Examine tree]
        //player gets small amount of gold, loses a bit of health
        As you peer at the tree it seems to stare back at you. Reaching out to touch the bark you realise that the bark is warm, there's a feeling, almost like pulsing. Looking into the hole you see a row of small wooden spikes, like teeth. Picking up a twig from the ground you poke it into the hole. Instantly the hole closes up snapping off the twig. Without warning a branch above you whips round and throws you into the air. Landing some distance from the tree you see some coins glittering in the undergrowth. These must have been dropped by other victims of the tree. Picking them up and dusting yourself off you vow not to look at strange trees again.
            {alterGold(10)}
            {alterHealth(-5)}
    *[Reach into hole]
       //player gets buff, loses health
        The tree seems to stare back at you as you lean in for a closer look at the hole. Inside the hole are what looks like wooden spikes, almost like teeth. Hesitantly you reach into the hole to touch a spike. As you do, there is a searing pain in your hand and without warning a branch above you whips round and throws you into the air. Landing with a thud some distance from the tree, you look at your hand seeing a small wooden spike sticking out of it while blood pools on the ground. Pulling it out you realise this spike may help you in coming battles. 
            {alterHealth(-10)}
            ~ Trinkets += Wooden_Spike
    *[Move on towards building]
    //loses minor health
        Edging round the tree you hear a rustling in the branches, almost like laughing and without warning a branch above you whips round and throws you into the air. Landing some distance from the tree you pick yourself up and vow to be more careful around strange trees in the future.
        {alterHealth(-2)}

- Continuing along the path it leads down a slight hill, and you can see an old wooden church in a grassy clearing. 
It has seen better days, the windows are crooked and they all have rusted metal bars over them, replacing glass that must have fallen out years ago. The steeple is crooked and collapsing on itself, parts of the roof gone... A large white tree is bursting through the roof near the back.
*[Examine Church]
~ enemy = "Zombie_Dog"
~ tempEnemyName = "{mobName(enemy)}" 

As you walk closer, you notice the stubs of gravestones dotted around in the grass, all the trees in the area are bent over, gnarled branches pointing towards the structure as if accusing it.
A large mangy frothing dog bounds around a corner, barking madly at you and baring its teeth in a threatening grimace, sloughs of flesh drop off the beast, as it bounds towards you.

**[Attack {tempEnemyName}!]
~ cardStory = tempEnemyName + " is dead"
# Fight #regular #normal #normal #normal
->Fight_Scene(->dead_zombie_dog) 


=dead_zombie_dog
The remains of the dog lie at your feet. It doesn't look like Zombie Dogs carry gold and you see nothing else of worth on the carcass. 

You move close to the church, there is a large doorless wooden arch as the main entrance. Even though part of the roof is missing it looks dark inside the church. The air is still and the silence is deafening. 
You can enter the church or move on.

****[Enter the church]
->church_interior

****[Move on]
->sludge
// Main entrance

=church_interior
~ image = "c1_church_interior"
~ enemy = "Church_Beast"
~ tempEnemyName = "{mobName(enemy)}" 
{The church interior is dim,with motes of dust catching the odd beam of light. You can hear the creaking of beams, and the jangling of metal. The smell of moulding wood and a underlying metallic odor assails your nostrils. Moving inside, from the high ceiling are numerous chains, moving slightly in the breeze, many have wicked looking hooks on. |At the far end of the church you can see the huge white tree, its branches thrusting out of the roof. The other is a raised area, with wooden steps, and an altar. } 
    {
    - not throw_bowl && not destroy_bowl: 
        The altar is draped with a thick, once red cloth, and upon the cloth is a small bowl. Around the bowl, placed in a radial pattern is a series of white twigs, all pointing towards the center. 
    - else: 
    <>The altar is draped with a thick, once red cloth.
    } 
    Behind the altar you can see the rickety steps that must lead up into the bell tower. 
TODO: If the bowl isn't destroyed / drunk can they investigate it again?
TODO:If they fight the church beast by the tree first, can they also fight it here?
TODO: Should there also be an option here to just leave the church without doing anything, or just doing one thing?
Will you...
{
- not place_bowl && not destroy_bowl:
    +[Investigate the bowl]
    ->investigate_bowl
}
{
- not get_mask:
+[Examine the tree]
->examine_tree  
}
*[Go up the tower?]
->up_tower


// Investigate the bowl.
=investigate_bowl

The bowl upon closer inspection has a lot of jagged looking sigils carved into it, the bowl itself contains a dark crusty looking liquid. You prod it lightly with an armored finger, and thick scarlet ooze squeezes through the cracks in the crust. A rattling of chains behind you causes you to startle, and you turn to look. You can see nothing in the gloom, some chains are shaking nearer the tree, as if something passed by them.

Would you like to
*[taste the foul looking liquid?]
    You raise the bowl to your lips, the smell of decay and mould assails your senses.
    **[Are you sure you want to taste it?]
        You tilt the bowl and a thick liquid pools out of the crust and dribbles down your throat. 
        It tastes disgusting, the flood of copper and decay fills your mouth.
        {alterHealth(-10)}
        {addCard(Blood_Of_Paradai)}
        Despite the taste and the retching, you feel slightly stronger. 
        ***[Place the bowl back]
        As you place the bowl down you <> -> place_bowl
    **[Place the bowl back] 
    You <> -> place_bowl
    

+(leave_bowl_alone)[Leave the bowl alone?]
->church_interior

*(destroy_bowl)[Cast the bowl to the floor and scatter the twigs?]
//Cast the bowl to the floor
You sweep the bowl to the floor, and it clangs on the flagstones spilling out the gorey ooze, you turn to scatter the twigs, but they are hovering above the alter, rotating slightly to match your movement, suddenly they fire off at you, striking you for x (15) damage.{alterHealth(-15)}
As you are recovering from this strange assault a bizarre looking creature stalks past the tree, through the chains towards you.
It resembles a person, walking on all fours, the legs are straight, back arched. Clad in swaithes of black cloth only its ivory coloured taloned hands and feet bare. A mask like face is sat, unmoving on its head... a fixed expression of twisted horror. The creature scurries closer, claws clicking on the stone floor,
    **[Attack {tempEnemyName}!]
    ~ cardStory = tempEnemyName + " is dead"
    # Fight #regular #normal #normal #normal
    ->Fight_Scene(->dead_church_beast) 

=place_bowl
hear the jangling again. Spinning round, there is a weird creature in the middle of the church, looking at you. 
It resembles a person, walking on all fours, the legs are straight, back arched. Clad in swaithes of black cloth only its ivory coloured taloned hands and feet bare. A mask like face is sat, unmoving on its head... a fixed expression of twisted horror. The creature scurries closer, claws clicking on the stone floor, bobbing its head up and down as if excited. 
It reaches the base of the altar where you stand, and looks up at you, head tilted quizzically. 
    
*[Attack {tempEnemyName}!]
~ cardStory = tempEnemyName + " is dead"
# Fight #regular #normal #normal #normal
->Fight_Scene(->dead_church_beast)     


*(throw_bowl)[Throw the bowl at the beast]
    You grab the bowl and throw it in the creatures face. The bowl bounces off the fixed visage of the beast and The sludge spills out and coats the weird mask. Shortly after, the twigs on the alter all twitch and rotate to face the spilled offering and one by one, they fire off into its face. The creature howls, spins around a few times and then attacks you. 
        
        **[Attack {tempEnemyName}!]
        ~ cardStory = tempEnemyName + " is dead"
        # Fight #mob_halfHealth #normal #normal #normal
        ->Fight_Scene(->dead_church_beast) 


*[Offer it the bowl]
    It moves up the steps towards you and seems to sniff at the bowl, in a flash it grabs the bowl and is burying its face into it, making lapping lapping and smacking noises and it gorges itself in a frenzy, finally sitting back. The bowl is clean.  
    The face of the creature is smeared with gore. Its body convulses and contorts, thrashing on the floor. Then with a swift movement, it leaps back onto all fours and attacks you.
        **[Attack {tempEnemyName}!]
        ~ cardStory = tempEnemyName + " is dead"
        # Fight #mob_twoStrength #normal #normal #normal
        ->Fight_Scene(->dead_church_beast) 

 
=dead_church_beast
Upon its death, the creature melts into the floor, leaving only the black cloth and an icor that drains into the gaps in the floor. You stagger away back towards the entrance to the church.
    ->church_loop
        

=examine_tree

You walk to the end of the church, chains clanking until you get to the end, the tree is magnificent. Pale and strong its branches have torn large holes in the roof. There are a lot of strange scratches around the base of the tree. Walking around to the other side you see a strange corpse lying on the floor, it is curled up in the fetal position, desiccated limbs bound in black cloth,{not get_mask: a weird fixed smile mask lying on top of it.  }
Will you go back into the main church area? Or pick up the mask? 
TODO: Will they get a second chance at picking up the mask?
+[Go back]
->church_interior
*(get_mask)[Pick up the mask]
TODO need to fix this. 
    {addCard(Church_Beasts_Mask)}

//if the player has not encountered the church beast.
{not dead_church_beast: 
    You pick up the mask, brushing off the dust, as you start to put it into your pack a movement up in the tree alerts you, you look up just in time to see another masked beast leaping down on you. 
    **[Attack {tempEnemyName}!]
    ~ cardStory = tempEnemyName + " is dead"
    # Fight #regular #normal #normal #normal
    ->Fight_Scene(->dead_church_beast) 


You pick up the mask, brushing off the dust. 

}

->church_loop


=up_tower
~ image = "c1_church_bell_tower"
//investigate steeple.
You negotiate the narrow rickety stairs and make your way upwards into the crooked dilapidated steeple.
You eventually make it to the top, there is a large rusted bell, resting at a strange slant against the side. The floor is also tilted at a crazy angle, on the floor opposite you perched on a tall stool, is a thin man, dressed in an old military coat, all buttons and medals. He is wearing a bicorn hat, and has a large bushy mustache with twirls at the end. 
*[Stare at the thin man]
“Well, well, it's about time you showed up, We are Huon of the East, once Captains of the kings own guard, it's been an age since we have had any business… Your going to challenge the king I presume?“ He smiles a toothy smile, “Well you are going to need all the help we can possibly give you.” reaches down and unfurls a bundle. Takes out a stout wooden box. “This here is the bell of Paradai, its one of our most prized possessions, But we can gift it to you… if you do one thing for us, there is a creature that lives in the big brother bell, just here in front of you… We wish for you to slay it for us. Do that and we will give you the little brother.”

Will you agree to slay the beast of the bell?, or decline?

    **(slay_the_beast)[Slay the beast]
    ->agree_slay_beast

    **[Decline]
    “Oh, that is most unfortunate, we are most disappointed, most would jump at the chance to gain such an artifact of power, you don't seem to be committed to this quest, are you sure you would not reconsider?”
        ***[Slay the beast]
        ->slay_the_beast
        ***(decline_fight)[Decline]
        {“Well, well, at least you are committed to your choices, that is something to admire, we suppose. Maybe that will be enough to help you through the ordeals ahead. At the very least you should look at our wares?”|Would you like to take another look at our wares?}
            ++++[Ask to see wares]
            -> Shop_Scene (->decline_fight, "bellShop")
            ****[Leave Tower]
            ->church_loop


//Agree
=agree_slay_beast
~ enemy = "Smoke_Beast"
~ tempEnemyName = "{mobName(enemy)}" 
“Excellent, we wish you luck… now if you would just step under and into the bell.”
You stoop down and enter the inside of the large bell, there is nothing inside. You start to crouch down to leave, when you hear Huon grunt and a resounding clang comes from outside the bell. At first nothing happens… then the noise starts to reverberate as the bell vibrates, the sound esculates and builds up to a painful pulsing. You clap your hands over your ears to try and block some of the immense noise but to no avail, your vision goes black and you collapse to the floor. 
*[Wake up]
    When you come to, you are lying sprawled on sandy ground. Climbing to your feet and  looking around all you can see is a vast featureless white sky, and the golden sands stretching off into the distance all around. Not far from you is a figure, seemingly made from a thick oily smoke. It turns to look at you and two golden eyes burn out of the evermoving coiling black smoke. Reaching out towards you with long black claws it drifts towards you. 
    
     **[Attack {tempEnemyName}!]
    ~ cardStory = tempEnemyName + " is dead"
        # Fight #mob_blockade #normal #normal #normal
        ->Fight_Scene(->dead_beast_of_bell) 

//Smoke beast fight. 
//Really high defense. Low hp.  (has blockade) 

=dead_beast_of_bell
{With your final blow, the creature lets out an unholy cry, the smoke billows out and enfolds you, when it dissipates you find yourself standing in the bell once more. You stoop down and exit the bell. Huon claps excitingly. “Well we are very pleased with that result, here is your gift.” He hands you a small golden bell, covered in sigils and patterns. “Use it well, So is there anything you would like to buy from us?” {addCard(Bell_Of_Paradai)}|Would you like to take another look?}
   
    Would you like to shop?
    +[Ask to see wares]
    -> Shop_Scene (->dead_beast_of_bell, "bellShop")
    *[Leave Tower]
    ->church_loop
    

=church_loop
{
- up_tower && examine_tree && dead_church_beast:
    Sensing there is nothing else for you in the church you <>
    ->sludge
- else:
    You head back towards the main area of the church. <>
    ->church_interior
}


=sludge
~ image = "c1_swamp"
~ enemy = "Wisp"
~ tempEnemyName = "{mobName(enemy)}" 
leave the church. As you walk back to the path, the trees in the area seem to bend towards you with deep creaking noises. They continue to follow you, eventually the trees start to thin out and the ground under your feet starts to become soft and boggy, the air is filled with a briny smell, the marsh stretches off into the distance. You can see the tops of the mountains far ahead of you, and the rooftops of the Harless Manor. Ahead you can see pools brackish water and twisted stubs of stripped trees with their roots exposed cling to the few remaining clumps of solid ground. 
You trudge through the sludge for several hours, thick clouds of midges and larger insects buzz around you, ahead you can see a strange rickety structure, a building on tall thin stilts.
*[Examine building]
    Walking closer you perceive a high pitch buzzing noise, a small ball of burning light races across the marsh towards you at incredible speed, it smacks into you knocking you off your feet into a dank pool. Getting to your feet, and wiping some of the muck off, you see the ball hovering right in front of you. The ball gives a tinny giggle, flares brightly, whizzes around you a few times then attacks..
    
     **[Attack {tempEnemyName}!]
    ~ cardStory = tempEnemyName + " is dead"
        # Fight #regular #normal #normal #normal
        ->Fight_Scene(->wisp_death) 
    
//wisp combat
//wisp does lots of light attacks and weakens player

=wisp_death
The wisp flickers and drops to the floor fizzling, water bubbling around it. Then it dulls, and goes out. 

*[Keep Walking]
~ image = "c1_stilt_building"
After some time you reach the base of the building on stilts, its several stories high, large bound logs forming the many legs. At its base piles of discarded bones and refuse. After examining it a while, you spot a ladder reaching up to the base of the structure.

    **[Will you climb the ladder and investigate?]
    ->investigate_stilts_building
    **[Or carry on?]
    ->manor_approach_east
//investigate. 
=investigate_stilts_building
~ image = "c1_stilt_building"
~ enemy = "Crow_Man"
~ tempEnemyName = "{mobName(enemy)}" 
At the top of the ladder, there is a square hole leading into the building, you haul yourself up and into it.  
Suddenly there is a flurry of feathers and loud cawing as many crows are startled by your appearance and flutter about the interior. They settle down on shelves and ledges, preening and eyeing you cautiously. 
The room is large, the walls are covered in shelving, bowing down under the weight of pieces of armour, chests, bits of random metal and a lot of general trash. At one end of the room is a vast nest, fashioned from large branches, made more comfortable with rugs, garments and large padded pillows. Reclined in the nest, is a huge obese man, he raises himself up to take a look at you, sweat streaming off his skin in the heat and the effort. 
*[Wait Silently]
    “What do you want?” He asks, his speech deep and slurred. “Oh, its you, my birds have been telling me of your progress, I have to admit I did not expect you to go get even this far.”
    He burps loudly, and gestures to the birds, one of them picks up a large strip of meat, flies over and lands on his chest and feeds the flesh into his maw.  
    “Well,” He says munching loudly, grease cascading down his front, “What would a repugnant and useless looking wretch like you want with the Baron of Cowanfil Morass? You here to act as a toilet for my crows? I shall inform my lord Sadul that another pretender has come to drool on his boots, ha ha mwaa ha!”
What would you like to do? Would you like to attack the gross Baron? Leave? Let him finish talking.
~ tempEnemyName = "{mobName(enemy)}" 
**[Attack {tempEnemyName}]
    //attack the Baron
    You move to attack, hand on your sword. “Woh woh, hang on there!” The Baron cries, raising his meaty arms up in defense. “No need for such feeble attempts at violence.” 
    You can attack him anyway, or stand down and let him finish talking. 
    ~ tempEnemyName = "{mobName(enemy)}" 
    ***[Attack {tempEnemyName}!]
    ->attack_baron
    
    ***[Stand down]
        //let him talk.
        “Pathetic weakling, I will send my children to the court, to bray out that there is another reject prospective usurper come to this land to stain its already sodden soil with worthless blood. Not even the lowest goblin will want to gnaw on your bones, the feeblest carrion will reject even your dullard eyes.”
        The horrendous man continues to labour insults upon you pushing your patience to its limit.
        ~ tempEnemyName = "{mobName(enemy)}" 
        ****[Attack {tempEnemyName}!]
        ->attack_baron
        ****[Leave]
        ->early_leave
        
**[Let him talk]
    //let him talk.
    “I will send my children to the court, to bray out that there is another reject prospective usurper come to this land to stain its already sodden soil with worthless blood. Not even the lowest goblin will want to gnaw on your bones, the feeblest carrion will reject even your dullard eyes.”
    The horrendous man continues to labour insults upon you pushing your patience to its limit.
    ~ tempEnemyName = "{mobName(enemy)}" 
    ***[Attack {tempEnemyName}!]
    ->attack_baron
    ***[Leave]
    ->early_leave

**[Leave]
->early_leave


=attack_baron
You draw your sword and advance on the immense man, as you start to get close, there is once again a huge flurry of birds, feathers flying and battering your helm. When it calms down a man stands between you and the Baron. He is constructed out a uncountable mash of crows, here and there you spy a beak, a foot and eye, its face is a fan of fluttering wings and it bears down on you, its mouth full of beaky teeth. 
    
    *[Attack Crow Man!]
    ~ cardStory = "Crow Man is dead"
    # Fight #regular #normal #normal #normal
    ->Fight_Scene(->crow_man_fight) 

//Crow man fight
=crow_man_fight
Your final blow shatters the crow man, the room goes black for a while as all the feathers explode outward and flutter down. 
The Baron lies, open mouthed, feathers stuck onto him, “Well, ahem, perhaps you have some teeth after all. I shall have to summon more crows… Spare me, and I shall give you useful information and maybe some objects that might assist you once you reach the royal quarters”

*[Spare the Baron]
->spare_baron
*[Run him through]
->run_him_through

=run_him_through
//run him through
{not crow_man_fight2:You} leap forward and drive your blade upto the hilt in the Barons flabby chest. Thick oily blood oozes out of the wound, he convulses, writhes in his bed, and then dissolves into a greasy liquid which pools out soaking the bedding and drains through the floor. 
You search the room, looking through all the trash the Baron had acquired… you manage to find {alterGold(10)} {addCard(Barons_Crow)}
    Finding nothing else you head to the ladder.
    ->leave_structure

=spare_baron
//Spare the baron
“Ok, listen here, it's still some way away, but you want to head to the Mountain gate, it's the only pass that will allow you access to the palace. There are no longer any gates, but there is certainly a gatekeeper. Here take this, he scrambles around and produces a medal, “This is the Grieving Heart, the gatekeeper will acknowledge it, but he has gone quite insane, it will buy you some time, that is all…“
    {addCard(Grieving_Heart)}
*[Wait for more information]
->stand_down
    
*[Climb down ladder]
You decide not to stay in case he starts talking again. 
->leave_structure

=early_leave
//attempt to leave. Lets give him a buff here
You move towards the ladder, as you do the man shouts after you.
“Ha, you have no conviction at all, do you? What possible threat could you have to the throne? Bah!” He spits at you with contempt. “I might as well put you out of your pitiful existence.” He claps his meaty hands and there is at once a huge flurry of birds, feathers flying and battering your helm. When it calms down a man stands between you and the Baron. He is constructed out of an uncountable mash of crows. Here and there you spy a beak, a foot and an eye. Its face is a fan of fluttering wings and as it bears down on you, you glimpse its mouth is full of beaky teeth. 
     **[Attack Crow Man!]
     ~ cardStory = "Crow Man is dead"
        # Fight #regular #normal #normal #normal
        ->Fight_Scene(->crow_man_fight) 

=crow_man_fight2
Your final blow shatters the crow man and once again the room goes black for a while as all the feathers explode outward and flutter down. Looking around you see the Baron cowering in a corner. Feeling fed up with these rediculous crow monstrosities you <> ->run_him_through

=stand_down
//
The Baron glances sideways at you and you realise he's about to start with his insults again. 
“You should have left when you had the chance, now I will reclaim my precious items." He spits at you with contempt. “I will put you out of your pitiful existence.” He claps his meaty hands and there is once again a huge flurry of birds, feathers flying and battering your helm. When it calms down once again a man made of crows stands between you and the Baron. 
    
     **[Attack Crow Man Again!]
     ~ cardStory = "Crow Man is dead"
        # Fight #regular #normal #normal #normal
        ->Fight_Scene(->crow_man_fight2) 

=leave_structure
You climb back down, and head towards the manor.
//need to put another section in here
->manor_approach_east

=manor_approach_east
~ image = "c1_manor_chapter_end"
Up ahead you can see the ground becoming firmer, the islands of solid ground more frequent and after a few more hours of walking you are back on solid ground the quagmire left behind you. You can see the manor ahead, tall walls and a iron gate. ->manor_goblin

=manor_goblin
As you get closer, you hear some noise in a nearby patch of gorse, a goblin leaps out, gnashing its crystalline teeth and frothing, it wastes no time, and attacks you. 
    ~ enemy = "Goblin"
     **[Attack {mobName(enemy)}!]
        # Fight #regular #normal #normal #normal
        ->Fight_Scene(->dead_manor_goblin) 

// Goblin fight. 
    =dead_manor_goblin
    Leaving the dead goblin in the gorse you approach the manor. As night starts to fall you realise this is a good time to make camp, you make a small fire and settle down for the night. As you drift off into sleep you think you hear a distant laughter, or maybe cackling.

->DONE















