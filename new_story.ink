INCLUDE branch1
INCLUDE branch2
INCLUDE branch3
INCLUDE branch4
INCLUDE branch5
INCLUDE branch6
INCLUDE randomencounters

EXTERNAL SetupMob(idx,name)

VAR setupFight = ""

{SetupMob(110,"'TYPE':'Bandit', 'NAME':'Test Bandit', 'SEQUENCE':1, 'OFFSET':0,'HP':100,'BUFFS':[],'DEBUFFS':[]")}
{SetupMob(111,"'TYPE':'Blob_Beast', 'NAME':'Blobby the Blob', 'SEQUENCE':1, 'OFFSET':0,'HP':103,'BUFFS':['thorns'],'DEBUFFS':[]")}
{SetupMob(112,"'TYPE':'Flour_Witch', 'NAME':'Test Flour Witch', 'SEQUENCE':1, 'OFFSET':0,'HP':100,'BUFFS':['regenerate'],'DEBUFFS':[]")}
{SetupMob(113,"'TYPE':'Giant_Pig', 'NAME':'Test Giant Pig', 'SEQUENCE':1, 'OFFSET':0,'HP':100,'BUFFS':[],'DEBUFFS':[]")}
{SetupMob(114,"'TYPE':'Goblin', 'NAME':'Test Goblin', 'SEQUENCE':1, 'OFFSET':0,'HP':100,'BUFFS':[],'DEBUFFS':[]")}
{SetupMob(115,"'TYPE':'Brazen_Filth', 'NAME':'Test Brazen Filth', 'SEQUENCE':1, 'OFFSET':0,'HP':100,'BUFFS':['thorns','regenerate'],'DEBUFFS':[]")}
{SetupMob(116,"'TYPE':'Wart', 'NAME':'Test Wart', 'SEQUENCE':1, 'OFFSET':0,'HP':100,'BUFFS':[],'DEBUFFS':[]")}
{SetupMob(117,"'TYPE':'Zombie_Dog', 'NAME':'Test Zombie Dog', 'SEQUENCE':1, 'OFFSET':0,'HP':100,'BUFFS':[],'DEBUFFS':[]")}
/*
Example fight setup
->Fight_Scene_Test("MOBS:['mob1',mob2'],CARDS:['normal','normal','normal'],CARDTXT:'Mob got annoyed and dropped out',INTROTXT:'yay! mudfight!',WINTXT:'Vanquished!',FAILTXT:'Failed!',TRANSITION:'transitionSimple'")->
*/      



LIST list_outsideEncounters = (enc1), (enc2), (enc3), (enc4), (enc5)

VAR next_one = "test"

LIST open_paths = (path1), (path2), path3, path4, path5, path6
LIST mobs_status = (intro_fight1), 
                    (intro_fight2), 
                    (intro_fight3),
                    (path1_fight1A),
                    (path1_fight1B),
                    (path1_fight1C),
                    (path1_fight2),
                    (path1_fight3), 
                    (path1_fight4),   
                    (path1_fight5), 
                    (path1_fight6),
                    (path1_fight7),
                    (path1_fight8),
                    (path2_fight1A), 
                    (path2_fight1B),
                    (path2_fight2A), 
                    (path2_fight2B),
                    (path2_fight2C),
                    (path2_fight3), 
                    (path2_fight4), 
                    (path2_fight5), 
                    (path3_fight1), 
                    (path3_fight2), 
                    (path3_fight3), 
                    (path3_fight4), 
                    (path3_fight5), 
                    (path3_fight6),
                    (path4_fight1), 
                    (path4_fight2), 
                    (path4_fight3), 
                    (path4_fight4), 
                    (path4_fight5),
                    (path4_fight6),
                    (path4_fight7),
                    (path4_fight8),
                    (path4_fight9),
                    (path5_fight1), 
                    (path5_fight2), 
                    (path5_fight3), 
                    (path5_fight4), 
                    (path5_fight5), 
                    (path5_fight6), 
                    (path6_fight1), 
                    (path6_fight2)

VAR introFight1 = "Brazen_Filth"
VAR introFight2 = "Goblin"
VAR introFight3 = "Goblin"

VAR branch1Fight1A = "Brazen_Filth"
VAR branch1Fight1B = "Brazen_Filth"
VAR branch1Fight1C = "Brazen_Filth"
VAR branch1Fight2 = "Wart"
VAR branch1Fight3 = "Goblin"
VAR branch1Fight4 = "Bandit"
VAR branch1Fight5 = "Vejio"
VAR branch1Fight6 = "Goblin"
VAR branch1Fight7 = "Zombie_Dog"
VAR branch1Fight8 = "Blob"

VAR branch2Fight1A = "Giant_Pig"
VAR branch2Fight1B = "Bandit"
VAR branch2Fight2A = "Goblins"
VAR branch2Fight2B = "Goblin"
VAR branch2Fight2C = "Goblin"
VAR branch2Fight3 = "Flour_Witch"


VAR branch3Fight1 = "Zombie_Dog"
VAR branch3Fight2 = "Zombie"
VAR branch3Fight3 = "Church_Beast"
VAR branch3Fight4 = "Smoke_Beast"
VAR branch3Fight5 = "Wisp"
VAR branch3Fight6 = "Crow_Man"

VAR branch4Fight1 = "Bandit"
VAR branch4Fight2 = "Butcher"
VAR branch4Fight3 = "Zombie"
VAR branch4Fight4 = "Bandit"
VAR branch4Fight5 = "Pickled_Zombie"
VAR branch4Fight6 = "Triffid"
VAR branch4Fight7 = "Garbage_Bison"
VAR branch4Fight8 = "Goblin"
VAR branch4Fight9 = "Wisp"

VAR branch5Fight1 = "Goblin"
VAR branch5Fight2 = "Bandit"
VAR branch5Fight3 = "Goma"
VAR branch5Fight4 = "Armoured_Guard"
VAR branch5Fight5 = "Goma"
VAR branch5Fight6 = "Necromancer"

VAR branch6Fight1 = "Goblin"
VAR branch6Fight2 = "Goma"

VAR random1 = "Bandit"
VAR random2 = "Goblin"
VAR random3 = "Wisp"
VAR random4 = "Murderer"

VAR what_happened = "died"
VAR in_inky = true
VAR health = 100

VAR mood = 50
VAR strength = 0
VAR dexterity = 0
VAR intelligence = 0
VAR luck = 0
VAR wisdom = 0
VAR constitution = 0
VAR charisma = 0

VAR maxHealth = 100
VAR path1_deathCount = 0
VAR path2_deathCount = 0
VAR path3_deathCount = 0
VAR path4_deathCount = 0
VAR path5_deathCount = 0
VAR path6_deathCount = 0
VAR crossroads_goblin1_deathcount = 0
VAR crossroads_goblin2_deathcount = 0
VAR brazen_filth_intro_deathcount = 0
VAR enemy = "empty"
VAR image = "empty"
VAR gold = 0
VAR tempGoldCounter = 0
VAR goldWon = 10
VAR cardSelected = "empty"
VAR cardStory = "empty"
VAR tempEnemyName = "empty"
VAR inkyEnemyName= "empty"
VAR resetPoint = ->introduction
VAR debugText = false
LIST completed_paths = completed_path1, completed_path2, completed_path3, completed_path4, completed_path5


->pre_intro
//{in_inky:->pre_intro|->introduction}
//->introduction

=== function SetupMob(idx,name) ===
~return ""

=== function returnHealthTextAfterFight ===
// the fight has left you ...
~ temp healthPercentage = health/maxHealth*100.0
{
- healthPercentage<10:
    {~badly hurt| bleeding badly|near deaths door}
- (health/maxHealth*100)<20:
        {~unsure if you can carry on|fearful of the future}
- health/maxHealth*100<30:
        health between 20 & 30%
- health/maxHealth*100<40:
        health between 40 & 40%
- health/maxHealth*100<50:
        health between 40 & 50%
- health/maxHealth*100<60:
        exhausted
- health/maxHealth*100<70:
        health between 60% & 70%
- health/maxHealth*100<80:
        health between 70% & 80%
- health/maxHealth*100<90:
        {~ with a throbbing headache|with sore arms}
- health/maxHealth*100<95:
        {~sore, but confident|with aching arms|with minor aches and pains}
- health/maxHealth*100<=100:
        {~feeling confident of your ability|confident of the future}
}


=== function giveRandomGold(x,y)
~ temp randomGoldAmount = RANDOM(x, y)
~ alterGold(randomGoldAmount)
~ return randomGoldAmount

=== function subtractRandomGold(x,y)
~ temp randomGoldAmount = RANDOM(x, y)
~ randomGoldAmount = 0 - randomGoldAmount
~ alterGold(randomGoldAmount)
~ return randomGoldAmount


=== function alterGold(x) ===
{
    - x == "all":
        ~ gold = 0
    - x >= gold:
        ~ gold = 0
    - else:
        ~gold = gold + x
}

=== function print_num(x) ===
{ 

    - x < 0:
        minus {print_num(0 - x)}
    - x >= 1000:
        {print_num(x / 1000)} thousand { x mod 1000 > 0:{print_num(x mod 1000)}}
    - x >= 100:
        {print_num(x / 100)} hundred { x mod 100 > 0:and {print_num(x mod 100)}}
    - x == 0:
        zero
    - else:
        { x >= 20:
            { x / 10:
                - 2: twenty
                - 3: thirty
                - 4: forty
                - 5: fifty
                - 6: sixty
                - 7: seventy
                - 8: eighty
                - 9: ninety
            }
            { x mod 10 > 0:<>-<>}
        }
        { x < 10 || x > 20:
            { x mod 10:
                - 1: one
                - 2: two
                - 3: three
                - 4: four        
                - 5: five
                - 6: six
                - 7: seven
                - 8: eight
                - 9: nine
            }
        - else:     
            { x:
                - 10: ten
                - 11: eleven       
                - 12: twelve
                - 13: thirteen
                - 14: fourteen
                - 15: fifteen
                - 16: sixteen      
                - 17: seventeen
                - 18: eighteen
                - 19: nineteen
            }
        }
}


=== function mobName(x) ===

{
    - x == "Brazen_Filth":
        {~Dirty|Ragtag|Shabby|Moth-Eaten|Scrawny|Mangy|Squalid} Brazen Filth
    - x == "Goblin":
        {~Scruffy|Dirty|Scrawny|Nasty|Mangy|Squalid|Moth-Eaten} Goblin
    - x == "Goblins":
        {~Scruffy|Dirty|Scrawny|Nasty|Mangy|Squalid|Moth-Eaten} Goblins
    - x == "Giant_Pig":
        ~ return "Giant Pig"    
    - x == "Bandit":
        {~Scruffy|Dirty|Ragtag|Shabby|Moth-Eaten} Bandit
    - x == "Flour_Witch":
        ~ return "Flour Witch"    
    - x == "Zombie_Dog":
        {~Large|Mangy|Frothing} Zombie Dog       
    - x == "Smoke_Beast":
        ~ return "Smoke Beast"   
    - x == "Wisp":
        ~ return "Wisp"    
    - x == "Crow_Man":
        Crow Man
    - x == "Butcher":
        ~ return "Butcher"
    - x == "Pickled_Zombie":
        ~ return "Pickled Zombie"
    - x == "Triffid":
        ~ return "Triffid"
    - x == "Garbage_Bison":
        ~ return "Garbage Bison"
    - x == "Goma":
        ~ return "Goma Acolyte"
    - x == "Armoured_Guard":
        ~ return "Armoured Guard"
    - x == "Necromancer":
        ~ return "Necromancer"
    - x == "Church_Beast":
         ~ return "Church Beast"
    - x == "Blob_Beast":
         {~Gooey|Slimy|Clammy} Blob
    - x == "Old_Man":
         ~ return "Old Man"
    - x == "Wart":
         ~ return "Wart"    
    - 
        ~ return x
} 

===alterHealth(x) ===
TODO remove text from this knot 
    {x} added to health. <>
    { health + x >= maxHealth:
        ~ health = maxHealth
        Health is at max: {health}
    - else:
        ~ health = health + x
        Health is: {health}
    }
    
    { health < 0:
        ~ health = 0
        ->You_Died
    }
->->    
  

===resetStats
// This resets health etc.. after dying, completing a branch 
Your health has been reset. 
~health = maxHealth
->->

===Fight_Scene_Test(setupString)
{not in_inky: 
    ~ setupFight = setupString
    <Fight>
    ->->
}
-
->Fight_Scene->
->->

===Fight_Scene
{not in_inky: ->->}
You are in Inky
You are fighting {tempEnemyName}. This is {inkyEnemyName}. Choose what happened 
    + Kill {tempEnemyName}
 //       ~ mobs_status -= inkyEnemyName
    
    + Die.
        ~health = 0
    -
    
    ~ gold = gold + 20
    
    {health <= 0: 
        ~ what_happened = "Died on {inkyEnemyName}"
        ->You_Died->
        ->resetStats->
        ->resetPoint
    }
-
->->

===You_Died
Oh dear, you seem to have died by {inkyEnemyName}. You have died {You_Died} time{|s}. Your health will now be reset and you'll be sent back to your reset point: {resetPoint}
->->


===randomManorEncounter
We don't have any random manor encounters yet
+[Continue]
-
->->

===randomOutsideEncounter
~next_one = LIST_RANDOM(list_outsideEncounters)
~list_outsideEncounters -= next_one

{
    - next_one == enc1:
        ->randomEncounters.outside_encounter1->
    - next_one == enc2:
        ->randomEncounters.outside_encounter2->
    - next_one == enc3:
        ->randomEncounters.outside_encounter3->
    - next_one == enc4:
        ->randomEncounters.outside_encounter4->
    - next_one == enc5:
        ->randomEncounters.outside_encounter5->
    - 
        Ran out of random encounters Random encounters are being reset. 
        ~ list_outsideEncounters = LIST_INVERT(list_outsideEncounters)
        ->randomOutsideEncounter
}

-
->->

===randomEncounter

->randomOutsideEncounter->

-
{debugText:You've completed the random encounter. Time to move on to the next location.}
->->

===shopEncounter
You're buying things in the shop
+[Done]

->->

===pre_intro
This is the menu before the game starts.
Debug text is {debugText:on|off} 

+[Go to Game]
    ->introduction
    
+(fight_test)[Fight Testing]
    ->resetStats->
    ++[Bandit]
            ~ enemy = branch1Fight4
            ~ tempEnemyName = "{mobName(enemy)}"
            ~ inkyEnemyName = path1_fight4
    ->Fight_Scene_Test("MOBS:[110],CARDS:['normal','normal','normal'],CARDTXT:'Mob got annoyed and dropped out',INTROTXT:'yay! mudfight!',WINTXT:'Vanquished!',FAILTXT:'Failed!',TRANSITION:'transitionSimple'")->
    
    
    ++[Blob]
            ~ enemy = branch1Fight8
            ~ tempEnemyName = "{mobName(enemy)}"
            ~ inkyEnemyName = path1_fight8
      ->Fight_Scene_Test("MOBS:[111],CARDS:['normal','normal','normal'],CARDTXT:'Mob got annoyed and dropped out',INTROTXT:'yay! mudfight!',WINTXT:'Vanquished!',FAILTXT:'Failed!',TRANSITION:'transitionSimple'")->
  
    
    ++[Flour Witch]
            ~ enemy = branch2Fight3
            ~ tempEnemyName = "{mobName(enemy)}"
            ~ inkyEnemyName = path2_fight3
      ->Fight_Scene_Test("MOBS:[112],CARDS:['normal','normal','normal'],CARDTXT:'Mob got annoyed and dropped out',INTROTXT:'yay! mudfight!',WINTXT:'Vanquished!',FAILTXT:'Failed!',TRANSITION:'transitionSimple'")->
  
    
    ++[Giant Pig]
            ~ enemy = branch2Fight1A
            ~ tempEnemyName = "{mobName(enemy)}"
            ~ inkyEnemyName = path1_fight1A
      ->Fight_Scene_Test("MOBS:[113],CARDS:['normal','normal','normal'],CARDTXT:'Mob got annoyed and dropped out',INTROTXT:'yay! mudfight!',WINTXT:'Vanquished!',FAILTXT:'Failed!',TRANSITION:'transitionSimple'")->
  
    
    ++[Goblin]
    ~ enemy = introFight2
    ~ tempEnemyName = "{mobName(enemy)}"
    ~ inkyEnemyName = intro_fight2
      ->Fight_Scene_Test("MOBS:[114],CARDS:['normal','normal','normal'],CARDTXT:'Mob got annoyed and dropped out',INTROTXT:'yay! mudfight!',WINTXT:'Vanquished!',FAILTXT:'Failed!',TRANSITION:'transitionSimple'")->
  
    
    
    ++[Inn Creature]
    ~ enemy = introFight1
    ~ tempEnemyName = "{mobName(enemy)}"
    ~ inkyEnemyName = intro_fight1
      ->Fight_Scene_Test("MOBS:[115],CARDS:['normal','normal','normal'],CARDTXT:'Mob got annoyed and dropped out',INTROTXT:'yay! mudfight!',WINTXT:'Vanquished!',FAILTXT:'Failed!',TRANSITION:'transitionSimple'")->
  
    
    ++[Wart]
            ~ enemy = branch1Fight2
            ~ tempEnemyName = "{mobName(enemy)}"
            ~ inkyEnemyName = path1_fight2
      ->Fight_Scene_Test("MOBS:[116],CARDS:['normal','normal','normal'],CARDTXT:'Mob got annoyed and dropped out',INTROTXT:'yay! mudfight!',WINTXT:'Vanquished!',FAILTXT:'Failed!',TRANSITION:'transitionSimple'")->
  
    
    ++[Zombie Dog]
            ~ enemy = branch3Fight1
            ~ tempEnemyName = "{mobName(enemy)}"
            ~ inkyEnemyName = path3_fight1
      ->Fight_Scene_Test("MOBS:[117],CARDS:['normal','normal','normal'],CARDTXT:'Mob got annoyed and dropped out',INTROTXT:'yay! mudfight!',WINTXT:'Vanquished!',FAILTXT:'Failed!',TRANSITION:'transitionSimple'")->
  
    
    ++Back to Debug Options
    ->pre_intro
    
    --Fight Finshed.
    ->fight_test
    
    
+[Debug Text Options]
    ++[Debug Text ON]
        ~ debugText = true
        ->pre_intro
    ++[Debug Text OFF]
        ~debugText = false
        ->pre_intro

+[Skip to chapter...]
Remember interactions may not be as expected if a previous encounter has beeng referenced and not done. 
    ++[DEBUG - Skip to old lady]
    -> Old_Lady
    
    ++[DEBUG - Skip to Chapter 1]
    ->Path_Choice.branch1
    
    ++[DEBUG - Skip to Chapter 2]
    ->Path_Choice.branch2
    
    ++[DEBUG - Skip to Chapter 3]
    ->Path_Choice.branch3
    
    ++[DEBUG - Skip to Chapter 4]
    ->Path_Choice.branch4
    
    ++[DEBUG - Skip to Chapter 5]
    ->Path_Choice.branch5
    
    ++[DEBUG - Skip to Chapter 6]
    ->Path_Choice.branch6
    
    ++Back to Debug Options
    ->pre_intro

+(test_random)[Testing random encounters]
    ++[Random Encounter]
    ->randomEncounter-> 
    ++[Encounter 1]
    ->randomEncounters.outside_encounter1->
    ++[Encounter 2]
    ->randomEncounters.outside_encounter2->
    ++[Encounter 3]
    ->randomEncounters.outside_encounter3->
    ++[Encounter 4]
    ->randomEncounters.outside_encounter4->
    ++Back to Debug Options
    ->pre_intro
    --
    You've finished the random encounter and are being returned to the debug menu.
    ->test_random


===pre_intro_debugOptions
Debug Options Play with debug text on or off:



    
+ [Testing random encounters]
    ++[Random Encounter]
    ->randomEncounter-> 
    ++[Encounter 1]
    ->randomEncounters.outside_encounter1->
    ++[Encounter 2]
    ->randomEncounters.outside_encounter2->
    ++[Encounter 3]
    ->randomEncounters.outside_encounter3->
    ++[Encounter 4]
    ->randomEncounters.outside_encounter4->
   
-
->pre_intro
    

    

===introduction
TODO: Change sex of random characters. Currently everyone apart from the old lady is male
{debugText: Debug text and options are turned on}
You were raised and trained for a singular purpose. Living on the fortified island all the other people around you were either there to serve, or to teach you. You learnt the ways of physical combat and use of powerful trinkets. Only one with the royal blood can vanquish the cursed king, remove the scourge of evil from the land and return it to its former glory. This is the task you have been assigned from birth.
After you were equipped with charms and runes of empowerment and protection, given a sturdy blade and a suit of armour you were ushered onto a small boat with a ritualistic celebration. The desperate hope clear on the faces of the crowd lining the water's edge, as you pushed off into the sea towards the mainland.

+ [Continue] Chapter 1 
    -> Chapter1_Intro
    




=Chapter1_Intro
~ image = "c1_start_landing"
~ enemy = introFight1
~ tempEnemyName = "{mobName(enemy)}"
~ inkyEnemyName = intro_fight1
Monstrous waves cast you violently to the rocky shore, smashing your small boat on the dilapidated quay. Shaken by the extreme force of your landing you struggle to your feet, the wind howling and lashing rain against your armour. You need to find cover to wait out the rest of the storm.

The closest structure you can make out in the briny spray and gusting sheets of water seems to be a small barn. A little further down you can just about see another structure, it might be an inn. A square sign outside batters against its post, wrenching to and fro in the gale. The only other vague cover you can see from this relentless weather is a stout tree, stripped of leaves and bent inland by the gale. 
You can stumble into the barn, attempt to gain cover by the tree or go a bit further and investigate the inn.
+(camp_by_tree)[Set up camp by the tree]
Stumbling under the force of the driving rain you reach the tree and collapse at its base, protected from the worst of the elements by its thick gnarled trunk. It is still bitterly cold, but the majority of the icy wind and rain is blocked by the branches, wrapping yourself in your sodden cloak you fall into a wearied sleep...
      TODO: Could we do an automatic page turn here to show the passing of time? 
A rustling noise awakens you and looking up you see a creature going through your bags, you sense another one somewhere behind you. Your sword is hidden in your cloak, slowly you start to draw it out and...
    ++[Attack the creature rifling through your bags]
    Thrusting your sword upwards you catch the creature unawares, causing it to bleed heavily... 
        // Fight with two mobs,one at half health
        ~ cardStory = tempEnemyName + " is dead"
        # Fight #mob_halfHealth #normal #normal #normal
        ->Fight_Scene-> 
    
    ++[Roll and attack the creature you sense behind you]
    As you roll to attack the second creature you realise it has moved more quickly than you anticipated. Before you get a chance to realign yourself it lashes out scratching you...
    // Fight with two mobs, player has bleed debuff
        ~ cardStory = tempEnemyName + " is dead"
        # Fight #debuffBleed #normal #normal #normal
        ->Fight_Scene->
    
    
    ++(tree_freeze)[Freeze, and wait for the creatures to make their move]
    Cautiously waiting until the creature has finished with your bags you jump to your feet and attack them
    // Regular fight with two mobs, 
    ~ cardStory = tempEnemyName + " is dead"
    # Fight #two #normal #normal #normal
        ->Fight_Scene->
    

+(enter_inn)[Enter the Inn]
Crashing into the inn, the sign announcing it as the broken flagon. You are greeted with silence.  A few dim guttering candles barely illuminate the place, slow slinking figures, drift into the shadows leaving you alone in the room.  The place has been ransacked, the bar smashed and only the jagged stumps of bottles remain. A few tables and rickety chairs are scattered about... The air is foul, a slight hint of mouldy ale, briny mud and subtle fishy tang. There is no hospitality here, no locals drinking seeking respite from the weather, no hot food to be had. You are exhausted. Will you attempt to find somewhere comfortable to sleep or give in to the temptation and just find a corner to pass out in. 
    ++[Look for a comfortable bed]
    You decide to make a bit more effort to find somewhere to rest yourself. After a bit of investigation, you find a rickety staircase partially concealed by a barricade of upturned tables. Once removed, you go up the stairs, and find several small rooms with bedrolls inside. You enter one and partially block the doorway by sliding a chest of drawers across, before passing out on a mildewy bedroll.
        TODO: Show night Passing
        +++[Continue]
        You are awoken by a sharp crack, a squeak of shifting old wood. A skinny ragged creature with large luminous eyes is climbing over your crude barricade, sharp stubs of teeth bared at you as it skulks nearer. Behind it you spy a multitude of the same creatures. Gnashing their teeth, they shrink back at the sight of naked steel as you draw your sword, apart from the one in the room with you who slinks closer and moves to attack.
            ++++[Attack set up 1.1 - Charge Creature]
            Pre fight text
            // Fight with two mobs,one at half health
            ~ cardStory = tempEnemyName + " is dead"
            # Fight #two_onehalfHealth #normal #normal #normal
            ->Fight_Scene-> 
            ++++[Attack set up 1.2 - Hold back and wait for creature to approach]
            Pre fight text
            // Regular fight with two mobs, 
            ~ cardStory = tempEnemyName + " is dead"
            # Fight #two #normal #normal #normal
            ->Fight_Scene->
            ++++[Attack set up 1.3 - Throw something at the creature]
            Pre fight text
            // Fight with two mobs, player has bleed debuff
            ~ cardStory = tempEnemyName + " is dead"
            # Fight #debuffBleed #normal #normal #normal
            ->Fight_Scene->
    
    ++[Sleep on a bench]
    It is light when you awake, the weather seems to have calmed. The sign creaks and water drips from the ceiling and down some of the walls. On the table next to you is a pint glass of light brown liquid, you suppose it could be ale. 
    Surrounding you are several crouched scrawny figures, with large luminous eyes. They startle and shift nervously back as you awake, grimacing revealing stubs of jagged teeth. One grabs a broken chair leg and brandishes it aggressively, while another steps closer to the table next to you. 
        +++[Attack the closest creature]
        Your sword knocks the glass off the table, causing the creature to scream in disappointment.
        // Regular fight with two mobs, 
        ~ cardStory = tempEnemyName + " is dead"
        # Fight #regular #normal #normal #normal
        ->Fight_Scene->
        +++[Throw the liquid over the closest creature]
        The creature screams as the liquid hits it's eyes, stumbling around in a daze 
        // Two mobs, one dazed, 
        ~ cardStory = tempEnemyName + " is dead"
        # Fight #two_oneDazed #normal #normal #normal
        ->Fight_Scene->
        +++[Grab the glass and down the contents]
        The liquid burns as you drink it, and gives you a feeling of instant strength
        // Fight with two mobs, player has 2 strength
        ~ cardStory = tempEnemyName + " is dead"
        # Fight #two_player2Strength #normal #normal #normal
        ->Fight_Scene-> 
    
    
    


+(enter_barn)[Push open the barn door]
    ~ image = "c1_barn"
    You crash into the barn, stumbling and almost falling over yourself as the large door gives way and lets you in. In the darkness you can make out few shapes. Finding some bales of soft mildewy hay you collapse exhausted and sleep overcomes you... ->alterHealth(5)->
       TODO: Could we do an automatic page turn here to show the passing of time? 
    A sudden deep thump wakes you. The barn is dimly lit by the dismal morning light, the shadow of two scrawny creatures with large luminous eyes passes over you as they creep towards their prey, snarling wetly they dart towards you.  
    You can attempt to fend off the creatures by charging or waiting for them to come to you, or make a run for it. 
    
     ++[Attack {tempEnemyName}!] 
        TODO Gabriel, can we make text appear here before the fight starts?
        ...grab your sword and take a swing at the figures hitting one before it reaches you.
        // Fight with two mobs,one at half health
    ~ cardStory = tempEnemyName + " is dead"
    # Fight #mob_halfHealth #normal #normal #normal
        ->Fight_Scene-> 
    
    ++[Hold your shield high and defend]
    As you hold your shield up, the two creatures charge at you...
    // Regular fight with two mobs, 
    ~ cardStory = tempEnemyName + " is dead"
    # Fight #regular #normal #normal #normal
        ->Fight_Scene->

    ++[Flee]
  
    The creatures are faster than you realise and jump into your path slicing at you with their claws causing you to lose blood. You have no choice but to attack them
        // Fight with two mobs, player has bleed debuff
        ~ cardStory = tempEnemyName + " is dead"
        # Fight #debuffBleed #normal #normal #normal
        ->Fight_Scene->
    
//This pulls back in from any of the above fights
- ->dead_brazen_filth
       
      
    
=dead_brazen_filth
{tree_freeze: Gathering up all the items the {mobName(enemy)} had removed from your bags you turn to examine the creatures. }
{enter_inn: The other creatures seem to melt into the scenery as you finish off their friends. }

Gingerly you poke the dead bodies with your sword, hesitant whether or not to search them for gold and useful items. Their flea ridden bodies almost seem alive due to the amount of small creatures which inhabit them. Would you like to investigate them further, or leave them alone?
    *Examine creatures
    Using your sword to pull away at the clothes on the creatures you discover a hidden pouch deep within the clothing of one of them. Opening it you find {print_num(goldWon)} gold coins which you quickly take. Hoping that you haven't picked up anything else more malignant you leave the filth <>
    *[Leave]
    Deciding against getting too close to the dead bodies and the creatures that inhabit them you leave the filth <>
-
{enter_barn:on the floor and glance out of the barn door. The storm has abated and you <>}
{enter_inn:in the dirt and confident that the rest of the inn's inhabitants won't follow, <>}
{camp_by_tree:lying on the sand. }
//{not tree:emerge into the daylight.} The fight has left you {returnHealthTextAfterFight()}, {health==maxHealth:and ready for the next challenge. | and you are glad of the fresh air to help clear your mind. }



->Old_Lady





===Old_Lady
~ image = "c1_old_lady_shelter"

{First visit to old lady| Subsequent visit to old lady having just {what_happened}}

+ Reset start point after death to here 
    ~ resetPoint= ->Old_Lady
    ~ what_happened = "set restart point to old lady"
    ->Old_Lady

+ Move to path choice 
    {mobs_status has intro_fight2: ->crossroads_goblins | ->Path_Choice}
  
    

=crossroads_goblins
~ image = "c1_cross_road"
~ enemy = introFight2
~ tempEnemyName = "{mobName(enemy)}"
~ inkyEnemyName = intro_fight2
{First visit here you will always be attacked by one goblin. | {&There is a decompsing body here, it looks familiar. There is a {tempEnemyName} eating the remains of the body | There are a couple of decompsing bodies here, they both look quite similar. There is a {tempEnemyName} digging through the remains. | There are a number of decomposing body parts here, it's unclear how many there are, there seem to be more arms than legs. There is a {tempEnemyName} sleeping amongst the remains| The path is now clear of body parts, you presume they have been eaten by the goblin. Looking around carefully you can see no sign of the goblin. ->Path_Choice |  {tempEnemyName} is crouched on the path, it looks hungry. All the decomposing body parts are gone }}

    {crossroads_goblin1_deathcount>0:. You have died {crossroads_goblin1_deathcount} time{|s} on this {tempEnemyName}}
+ [Attack {& the {tempEnemyName}| the {tempEnemyName} as it eats the remains| the {tempEnemyName} while it's distracted| as it sleeps| the {tempEnemyName}} ]
    # Fight #mob_halfHealth #normal #normal #normal #
    ~crossroads_goblin1_deathcount++
    You pull out your sword and challenge the {tempEnemyName}
    ->Fight_Scene->
+ [Defend yourself against {& the {tempEnemyName}| the {tempEnemyName} as it eats the remains| the {tempEnemyName} while it's distracted| as it sleeps| the {tempEnemyName}} ]
    # Fight #regular #normal #normal #normal
    ~crossroads_goblin1_deathcount++
    ->Fight_Scene->
+ [Run at {& the {tempEnemyName}| the {tempEnemyName} as it eats the remains| the {tempEnemyName} while it's distracted| as it sleeps| the {tempEnemyName}} ]
    # Fight #mob_dazed #normal #normal #normal
    ~crossroads_goblin1_deathcount++
    ->Fight_Scene->


- 
~crossroads_goblin1_deathcount--
->Path_Choice

 

=end_crossroads_goblin_fight1
~ enemy = introFight3
~ tempEnemyName = "{mobName(enemy)}" + "'s"
~ inkyEnemyName = intro_fight3
With your final blow the paltry Goblin collapses to the ground at your feet, the others cringe away from you and slink away into the bushes. You can feel their eyes on you as you wipe the goblins blood from your sword. The fight has left you {returnHealthTextAfterFight()} and you wonder how many more of these creatures stand between you and your goal. Looking down at the body you are unsure if you should examine it further or leave the area before the other Goblins become braver.
To see this text you must have killed the first goblin but not the second. Here are some options:
    * (examine_goblin)Examine the goblins body
        Digging through the goblins rags you find some chewed gold coins. Just as you are putting them in your backpack there is a noise from behind you. Too late you realise there must be more of these creatures.
            ->crossroads_goblins2
    * (read_signpost)Read the signpost
        The sign tells you some stuff. Behind you, there is a rustling in the bushes. Turning around slowly you see two smaller goblins, they are eying you up warily. 
            ->crossroads_goblins2 

=crossroads_goblins2
~ image = "c1_cross_road"
This is the first time the two {tempEnemyName} have attacked you. You came from {examine_goblin: examining the goblin| reading the signpost}

    +Attack by rolling and thrusting your sword towards the {examine_goblin: noise | {tempEnemyName} }
        ~crossroads_goblin2_deathcount++
        # Fight #mob_halfHealth #normal #normal #normal #
    ->Fight_Scene->
    +Defend with your shield
        ~crossroads_goblin2_deathcount++
        # Fight #regular #normal #normal #normal
        ->Fight_Scene->
    +Run at the {examine_goblin: noise | {tempEnemyName} }
        ~crossroads_goblin2_deathcount++
        # Fight #mob_dazed #normal #normal #normal
        ->Fight_Scene->

- 
~crossroads_goblin2_deathcount--
->end_crossroads_goblin_fight2

=crossroads_goblin_fight2
~ image = "c1_cross_road"
~ enemy = introFight3
~ tempEnemyName = "{mobName(enemy)}" + "'s"
~ inkyEnemyName = intro_fight3
{This is the second time you've come through here. There are two small {tempEnemyName} standing on the path eating a corpse so you can't examine the signpost or the old goblin body | Third time round | fourth time round |Too many to count times round}

{crossroads_goblin2_deathcount>0:. You have died {crossroads_goblin2_deathcount} time{|s} on these {tempEnemyName}}

    +Attack
    ~crossroads_goblin2_deathcount++
        # Fight #mob_halfHealth #normal #normal #normal #
    ->Fight_Scene->
    +Defend with your shield
    ~crossroads_goblin2_deathcount++
        # Fight #regular #normal #normal #normal
        ->Fight_Scene->
    +Run at the {tempEnemyName} 
    ~crossroads_goblin2_deathcount++
        # Fight #mob_dazed #normal #normal #normal
        ->Fight_Scene->

- 
~crossroads_goblin2_deathcount--
->end_crossroads_goblin_fight2


=end_crossroads_goblin_fight2
~ image = "c1_cross_road"
You quickly search the Goblins body taking the {print_num(goldWon)} coins that you find. Looking up at the signpost you know you need to make a decision.

->Path_Choice



=signpost_details
~ image = "c1_cross_road"
+ Examine signpost
    {open_paths has path1:
        To the East you can see some stuff, it looks quite sandy it is branch 1. <>
    }
     {open_paths has path2:
        To the West it looks pretty bland it is branch 2. 
    }

    {LIST_COUNT (open_paths)==1: <> Lying on the floor is a directional sign. The writing has been scratched out. }
    
    {open_paths has path3:
        To the East is branch 3. {It looks different to last time you looked that way. | You prefered it when it was sandy. }  
    }
     {open_paths has path4:
        <> To the West is branch 4. {It's not the same as it used to be. |The clouds look grey}  
    }
    {open_paths has path5:
       <> To the {open_paths has path3: West} {open_paths has path4: East} is branch 5
        }
   - ->Path_Choice   
        



===Path_Choice
~ image = "c1_cross_road"
{mobs_status hasnt intro_fight2 && !Old_Lady.end_crossroads_goblin_fight1:<-Old_Lady.end_crossroads_goblin_fight1| {mobs_status hasnt intro_fight2 && mobs_status has intro_fight3:->Old_Lady.crossroads_goblin_fight2}}
{mobs_status hasnt intro_fight3: <-Old_Lady.signpost_details}
{First visit to path choice| subsequent visit to path choice}
{open_paths: Paths open are: {open_paths} | There are no paths open}
    {open_paths has path1:
        Path1 is to the East
        + [Go East to branch1{path1_deathCount>0:. You have died {path1_deathCount} times on this path}]
            ->branch1
            }
     {open_paths has path2:
        Path2 is to the West
        + [Go West to branch2{path2_deathCount>0:. You have died {path2_deathCount} times on this path}]
            ->branch2
            }    
    
     {open_paths has path3:
        Path3 is to the East
        + [Go East to branch3{path3_deathCount>0:. You have died {path3_deathCount} times on this path}]
            ->branch3
            }

     {open_paths has path4:
        Path4 is to the West
        + [Go West to branch4{path4_deathCount>0:. You have died {path4_deathCount} times on this path}]
            ->branch4
            }

     {open_paths has path5:
        TODO: Need to fix the bug here about path5 not having a direction and setting a variable with the direction in for the text
        Path5 is on the {open_paths has path3: west} {open_paths has path4: east}
        + [Go {open_paths has path3: West} {open_paths has path4: East} to branch5{path5_deathCount>0:. You have died {path5_deathCount} times on this path}]
            
            ->branch5
            }
            
     {open_paths has path6:
        Path6 is straight on
        + [Go straight on to branch6]
            ->branch6
            }
            
        {open_paths has path1 && open_paths hasnt path2:
            The path to the West is blocked by vines
        }
        
        {open_paths has path2 && open_paths hasnt path1:
            The path to the East is blocked by vines
        }

=branchBypass(tempBranchNo)
// This knot allows QA to quickly complete a branch if needed
Branch Bypass available
{   
    - tempBranchNo == 1:
        + [DEBUG - Die on branch1]
            ~what_happened = "died on branch " + tempBranchNo
            ~path1_deathCount++
            -> Old_Lady
        + (branch1_completed)[DEBUG - Complete branch1]
            ->CompletedBranch(1)
            
    - tempBranchNo == 2:
        + [DEBUG - Die on branch2]
            ~what_happened = "died on branch 2"
            ~path2_deathCount++
            -> Old_Lady
        + (branch2_completed)[DEBUG - Complete branch2]
            ->CompletedBranch(2)
            
    - tempBranchNo == 3:
        + [DEBUG - Die on branch3]
            ~what_happened = "died on branch 3"
            ~path3_deathCount++
            -> Old_Lady
        + (branch3_completed)[DEBUG - Complete branch3]
            ->CompletedBranch(3)
    
    - tempBranchNo == 4:
        + [DEBUG - Die on branch4]
            ~what_happened = "died on branch 4"
            ~path4_deathCount++
            -> Old_Lady
        + (branch4_completed)[DEBUG - Complete branch4]
            ->CompletedBranch(4)
    
    - tempBranchNo == 5:
        + [DEBUG - Die on branch5]
            ~what_happened = "died on branch 5"
            ~path5_deathCount++
            -> Old_Lady
        + (branch5_completed)[DEBUG - Complete branch5]
            ->CompletedBranch(5)

    - tempBranchNo == 6:
        + [DEBUG - Die on branch6]
            ~what_happened = "died on branch 6"
            ~path6_deathCount++
            -> Old_Lady
        +(branch6_completed)[DEBUG - Complete final branch]
        ->END
            
}

=CompletedBranch(tempBranchCompleted)
// Actions when branch completed

{   
    - tempBranchCompleted == 1:
        ~what_happened = "completed branch 1"
        ~ open_paths -= path1
        {open_paths hasnt path2: 
            ~open_paths += (path3, path4)
        }
        ->resetStats->
        -> Old_Lady
        
    - tempBranchCompleted == 2:
        ~what_happened = "completed branch 2"
            ~ open_paths -= path2
            {open_paths hasnt path1: 
                ~open_paths += (path3, path4)
            }
        ->resetStats->
        -> Old_Lady

    - tempBranchCompleted == 3:
        ~completed_paths += completed_path3
            ~what_happened = "completed branch 3"
            ~ open_paths -= path3
            {
                -completed_paths has completed_path4 && completed_paths has completed_path5: 
                    ~ open_paths += path6 
                - else:
                    ~ open_paths += path5 
            } 
            ->resetStats->
            -> Old_Lady
            
    - tempBranchCompleted == 4:
    ~completed_paths += completed_path4
            ~what_happened = "completed branch 4"
            ~ open_paths -= path4
            {
                -completed_paths has completed_path3 && completed_paths has completed_path5:  
                    ~ open_paths += path6 
                - else:
                    ~ open_paths += path5 
            }  
            ->resetStats->
            -> Old_Lady
            
    - tempBranchCompleted == 5:
        ~completed_paths += completed_path5
            ~what_happened = "completed branch 5"
            ~ open_paths -= path5
            {
                -completed_paths has completed_path3 && completed_paths has completed_path4: : 
                    ~ open_paths += path6 
            } 
            ->resetStats->
            -> Old_Lady
}
    

=branch1
{debugText:<-branchBypass(1)}
->branch1subfile->
->CompletedBranch(1)


=branch2
{debugText:<-branchBypass(2)}
->branch2subfile->
->CompletedBranch(2)   


=branch3
{debugText:<-branchBypass(3)}
->branch3subfile->
->CompletedBranch(3)   


=branch4
{debugText:<-branchBypass(4)}
->branch4subfile->
->CompletedBranch(4)

=branch5
{debugText:<-branchBypass(5)}
->branch5subfile->
->CompletedBranch(5)


=branch6
{debugText:<-branchBypass(6)}
->branch6subfile->

-
You've finished the game
->END
